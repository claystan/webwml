#use wml::debian::template title="Debian GNU/Hurd --- Ontwikkeling" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="1ee233788cf476e1bd555018a37476e483d815f4"

<h1>
Debian GNU/Hurd</h1>
<h2>
De ontwikkeling van de distributie</h2>

<h3>
Debian pakketten geschikt maken</h3>
<p>
Als u de overzetting van Debian naar GNU/Hurd wilt helpen, moet u zichzelf
vertrouwd maken met het Debian-verpakkingssysteem. Eens u dit gedaan heeft door
de beschikbare documentatie te lezen en de <a href="$(HOME)/devel/">Hoek voor
Ontwikkelaars</a> te bezoeken, zou u moeten weten hoe u Debian
broncodepakketten uitpakt en een Debian pakket bouwt. Hier volgt een
spoedcursus voor de zeer luie mensen:</p>

<h3>
Broncode verkrijgen en pakketten bouwen</h3>

<p>
De broncode verkrijgen kan door eenvoudig <code>apt source package</code> uit
te voeren, dat ook de broncode zal uitpakken.
</p>

<p>
Voor het uitpakken van een broncodepakket van Debian is het bestand
<code>package_version.dsc</code> vereist en de erin vermelde bestanden.
U bouwt de compilatiemap van Debian met het commando
<code>dpkg-source -x package_version.dsc</code>
</p>

<p>
Het bouwen van een pakket gebeurt in de nu bestaande compilatiemap van de Debian
<code>pakket-versie</code> met het commando
<code>dpkg-buildpackage -B "-mMijnNaam &lt;MijnEmail&gt;"</code>.
In plaats van <code>-B</code> kunt u
<code>-b</code> gebruiken, als u ook de architectuuronafhankelijke delen van
het pakket wilt bouwen. U kunt
<code>-uc</code> toevoegen om te vermijden dat het pakket ondertekend wordt met
uw pgp-sleutel.</p>

<p>
Voor het bouwen zijn mogelijk extra geïnstalleerde pakketten nodig. De
eenvoudigste manier is om <code>apt build-dep package</code> uit te voeren, dat
alle vereiste pakketten zal installeren.
</p>

<p>
Het gebruik van pbuilder kan handig zijn. Het kan gebouwd worden met
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>
en dan kan men <code>pdebuild -- --binary-arch</code> gebruiken dat het
downloaden van de bouwvereisten zal afhandelen, enz. en het resultaat in
<code>/var/cache/pbuilder/result</code> zal plaatsen.
</p>

<h3>
Er een uitkiezen</h3>
<p>
Aan welk pakket gewerkt moet worden? Wel, elk pakket dat nog niet overgezet
werd, maar wel overgezet moet worden.  Dit verandert constant, dus is het beter
om u eerst te concentreren op pakketten die door veel andere pakketten vereist
worden, hetgeen te zien is in de grafiek met pakketvereisten
<url "https://people.debian.org/~sthibault/graph-radial.pdf">,
die elke dag bijgewerkt wordt,
of op de lijst met de meest gevraagde pakketten
<url "https://people.debian.org/~sthibault/graph-total-top.txt">
(dit is de lijst met veelgevraagde pakketten op de lange termijn; de lijst met
veelgevraagde pakketten op de korte termijn is
<url "https://people.debian.org/~sthibault/graph-top.txt">).
Het is meestal ook een goed idee om te kiezen uit de lijsten met verouderde
pakketten
<url "https://people.debian.org/~sthibault/out_of_date2.txt"> en
<url "https://people.debian.org/~sthibault/out_of_date.txt">,
omdat die vroeger werkten en nu waarschijnlijk om slechts een paar redenen
defect zijn. U kunt ook gewoon willekeurig één van de ontbrekende pakketten
kiezen, of letten op de autobuild-logs op de mailinglijst van het bouwlogboek
van debian-hurd, of gebruik maken van de wanna-build-lijst van
<url "https://people.debian.org/~sthibault/failed_packages.txt">.
Sommige bouwproblemen zijn gemakkelijker op te lossen dan andere. Een typisch
probleem is "undefined reference to foo", waarbij foo zoiets is als
pthread_create, dlopen, cos, ... (die uiteraard beschikbaar zijn op hurd-i386),
wat alleen laat zien dat men in de configuratiestap van het pakket vergeten is
om ook op de Hurd -lpthread, -ldl, -lm, enz. op te nemen. Houd er echter
rekening mee dat ALSA MIDI-functies niet beschikbaar zijn.
</p>
<p>
Controleer ook of er al werk gedaan werd op
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">,
en op het BTS (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">), en op <url "https://wiki.debian.org/Debian_GNU/Hurd">,
en de levensstatus van pakketten op buildd.debian.org, bijv.
<url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Pakketten die niet overgezet zullen worden</h4>
<p>
Sommige van deze pakketten, of delen ervan, kunnen later overzetbaar worden,
maar op zijn minst op dit moment worden deze als niet-overzetbaar beschouwd. Ze
worden normaal gemarkeerd als NotForUs in de database van buildd.
</p>

<ul>
<li>
<code>base/makedev</code>, omdat de Hurd met zijn eigen versie van dit script
komt. Het Debian bronpakket bevat alleen een Linux-specifieke versie.</li>
<li>
<code>base/modconf</code> en <code>base/modutils</code>, omdat modules een
concept zijn dat specifiek is voor Linux.</li>
<li>
<code>base/netbase</code>, omdat wat daar nog rest, zeer specifiek is voor de
Linux-kernel. In plaats daarvan gebruikt de Hurd
<code>inetutils</code>.</li>
<li>
<code>base/pcmcia-cs</code>, omdat dit pakket Linux-specifiek is.</li>
<li>
<code>base/setserial</code>, omdat dit specifiek is voor de Linux-kernel. Maar
met de overzetting van Linux char-stuurprogramma's naar GNU Mach kunnen we dit
misschien gebruiken.</li>
</ul>

<h3> <a name="porting_issues">
Algemene overdrachtsproblemen</a></h3>
<p>
<a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>Een
lijst met veelvoorkomende problemen</a> is te vinden op de bovenstroomse
website. De volgende veelvoorkomende problemen zijn specifiek voor Debian.</p>
<p>Voordat u iets probeert op te lossen, moet u nagaan of de overzetting naar
kfreebsd* niet al een oplossing heeft die gewoon een uitbreiding naar hurd-i386
nodig heeft.</p>

<ul>
<li>
<code>Defecte libc6-vereiste</code>
<p>
Sommige pakketten gebruiken een foutieve afhankelijkheid van
<code>libc6-dev</code>. Dit is onjuist omdat <code>libc6</code> specifiek is
voor sommige architecturen van GNU/Linux. Het corresponderende pakket voor GNU
is <code>libc0.3-dev</code>, maar andere besturingssystemen zullen er een ander
hebben. U kunt het probleem vinden in het bestand <code>debian/control</code>
van de broncodeboom. Typische oplossingen zijn onder meer het detecteren van
het besturingssysteem met behulp van <code>dpkg-architecture</code> en de soname
in de code verankeren, of beter, een logische OR gebruiken, bijv.:
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
De vermelding <code>libc-dev</code> betreft een virtueel pakket dat voor elke
soname werkt, maar u moet deze optie enkel als laatste plaatsen.</p></li>
<li>
<code>ongedefinieerde verwijzing naar snd_*, SND_* niet gedeclareerd</code>
<p>
Sommige pakketten gebruiken ALSA zelfs op niet-Linux architecturen. Het pakket
oss-libsalsa biedt enige emulatie over OSS, maar het is beperkt tot 1.0.5, en
sommige functies zijn niet beschikbaar, zoals alle sequencer-bewerkingen.
</p>
<p>
Als het pakket dit toelaat, zou alsa-ondersteuning moeten worden uitgeschakeld
op de architecturen <code>!linux-any</code> (bijvoorbeeld via een optie voor
<code>configure</code>), en een kwalificatie <code>[linux-any] </code> zou
toegevoegd moeten worden aan de <code>Build-Depends</code> van alsa, en het
omgekeerde zou moeten toegevoegd worden aan <code>Build-Conflicts</code>, zoals
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
<code>dh_install: Kan geen (overeenkomsten voor) "foo" vinden (geprobeerd in ., debian/tmp)</code>
<p>
Dit gebeurt meestal wanneer de bovenstroomse code iets niet installeerde omdat
die het besturingssysteem niet herkende. Soms is de code gewoon dom (ze weet
bijvoorbeeld niet dat het bouwen van een gedeelde bibliotheek op GNU/Hurd
precies hetzelfde is als op GNU/Linux) en dat moet worden gerepareerd. Soms is
het echt logisch (bijv. Systemd-servicebestanden niet installeren). In dat
geval kan men dh-exec gebruiken: een bouwvereiste instellen op <tt>dh-exec</tt>,
het commando <tt>chmod +x</tt> uitvoeren voor het bestand <tt>.install</tt> en
aan het begin van de problematische lijnen bijv. <tt>[linux-any]</tt> of
<tt>[!hurd-any]</tt> invoegen.
</p>

<h3> <a name="debian_installer">
Ontwikkelen van het Debian-installatieprogramma</a></h3>

<p>
De eenvoudigste manier om een ISO-image te bouwen is te beginnen met een
bestaand image van de pagina met <a href=hurd-cd>Hurd cd-images</a>. U kunt dit
dan aankoppelen en kopiëren:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/myimage
umount /mnt
chmod -R +w /tmp/myimage
</pre></td></tr></table>

<p>
U kunt de initiële ram-schijf aankoppelen, en bijv. een translator (vertaler)
vervangen door uw eigen versie:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/myimage/initrd.gz
mount /tmp/myimage/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/myimage/initrd
</pre></td></tr></table>

<p>
Nu kunt u het ISO-image opnieuw bouwen met grub-mkrescue:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/myimage/boot/grub/i386-pc
grub-mkrescue -o /tmp/myimage.iso /tmp/myimage
</pre></td></tr></table>

</li>
</ul>

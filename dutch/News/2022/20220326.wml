#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.3 werd uitgebracht</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de derde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van stable, de stabiele release, voegt een paar belangrijke
correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apache-log4j1.2 "Oplossen van beveiligingsproblemen [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307], door ondersteuning voor de modules JMSSink, JDBCAppender, JMSAppender en Apache Chainsaw te verwijderen">
<correction apache-log4j2 "Oplossen van een probleem met code-uitvoering vanop afstand [CVE-2021-44832]">
<correction apache2 "Nieuwe bovenstroomse release; crash als gevolg van willekeurig lezen van geheugen repareren [CVE-2022-22719]; probleem met smokkelen van HTTP-verzoeken oplossen [CVE-2022-22720]; oplossen van problemen van schrijven buiten het bereik [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Probleem met een informatielek oplossen [CVE-2021-46671]">
<correction base-files "Update voor tussenrelease 11.3">
<correction bible-kjv "Oplossen van een fout van het type off-by-one in de zoekfunctie">
<correction chrony "Het lezen van het chronyd-configuratiebestand dat timemaster(8) genereert, toestaan">
<correction cinnamon "Crash verhelpen bij het toevoegen van een online account met login">
<correction clamav "Nieuwe bovenstroomse stabiele release; probleem met denial of service oplossen [CVE-2022-20698]">
<correction cups-filters "Apparmor: lezen van Debian Edu's cups-browsed configuratiebestand toestaan">
<correction dask.distributed "Ongewenst luisteren door werkers op publieke interfaces oplossen [CVE-2021-42343]; oplossen van compatibiliteit met Python 3.9">
<correction debian-installer "Hercompilatie tegen proposed-updates; linux kernel-ABI updaten naar 5.10.0-13">
<correction debian-installer-netboot-images "Hercompilatie tegen proposed-updates">
<correction debian-ports-archive-keyring "Toevoegen van <q>Automatische ondertekeningssleutel van het Debian Ports-archief (2023)</q>; de ondertekeningssleutel 2021 verplaatsen naar de sleutelbos met verwijderde sleutels">
<correction django-allauth "OpenID-ondersteuning oplossen">
<correction djbdns "De datalimiet voor axfrdns, dnscache en tinydns optrekken">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction e2guardian "Validatieprobleem met ontbrekend SSL-certificaat oplossen [CVE-2021-44273]">
<correction epiphany-browser "Een bug in GLib omzeilen om een crash van het UI-proces op te lossen">
<correction espeak-ng "Ongewenste vertraging van 50 ms tijdens het verwerken van gebeurtenissen weglaten">
<correction espeakup "debian/espeakup.service: espeakup beschermen tegen systeemoverbelasting">
<correction fcitx5-chinese-addons "fcitx5-table: ontbrekende vereisten fcitx5-module-pinyinhelper en fcitx5-module-punctuation toevoegen">
<correction flac "Oplossing voor een probleem van schrijven buiten het bereik [CVE-2021-0561]">
<correction freerdp2 "Extra loggen voor foutopsporing uitschakelen">
<correction galera-3 "Nieuwe bovenstroomse release">
<correction galera-4 "Nieuwe bovenstroomse release">
<correction gbonds "Treasury API gebruiken voor aflossingsdata">
<correction glewlwyd "Mogelijke bevoegdheidsescalatie oplossen">
<correction glibc "Reparatie voor slechte omzetting van ISO-2022-JP-3 met iconv [CVE-2021-43396]; oplossen van problemen van bufferoverloop [CVE-2022-23218 CVE-2022-23219]; probleem van gebruik na vrijgave oplossen [CVE-2021-33574]; ophouden met het vervangen van oudere versies van /etc/nsswitch.conf; vereenvoudigde controle op ondersteunde kernelversies, aangezien 2.x-kernels niet langer worden ondersteund; ondersteuning van installatie op kernels met een releasenummer groter dan 255">
<correction glx-alternatives "Na het initiële instellen van de omleidingen, een minimaal alternatief voor de omgeleide bestanden installeren, zodat er geen bibliotheken ontbreken voordat glx-alternative-mesa zijn triggers verwerkt heeft">
<correction gnupg2 "scd: reparatie van het CCID-stuurprogramma voor SCM SPR332/SPR532; netwerkinteractie vermijden in de generator, wat tot vastlopen kan leiden">
<correction gnuplot "Oplossing voor een deling door nul [CVE-2021-44917]">
<correction golang-1.15 "Reparatie van IsOnCurve voor big.Int-waarden die geen geldige coördinaten zijn [CVE-2022-23806]; math/big: groot geheugengebruik voorkomen in Rat.SetString [CVE-2022-23772]; cmd/go: voorkomen dat takken zich ontwikkelen tot versies [CVE-2022-23773]; stapeluitputting door het compileren van diep geneste uitdrukkingen oplossen [CVE-2022-24921]">
<correction golang-github-containers-common "Bijwerken van seccomp-ondersteuning om het gebruik van nieuwere kernelversies mogelijk te maken">
<correction golang-github-opencontainers-specs "Bijwerken van seccomp-ondersteuning om het gebruik van nieuwere kernelversies mogelijk te maken">
<correction gtk+3.0 "Reparatie voor ontbrekende zoekresultaten bij gebruik van NFS; voorkomen dat de verwerking van het Wayland-klembord vastloopt in bepaalde uitzonderlijke gevallen; het afdrukken naar door mDNS gevonden printers verbeteren">
<correction heartbeat "Aanmaak van /run/heartbeat repareren op systemen met systemd">
<correction htmldoc "Oplossing voor een probleem met lezen buiten de grenzen [CVE-2022-0534]">
<correction installation-guide "Bijwerken van documentatie en vertalingen">
<correction intel-microcode "Update van ingesloten microcode; enkele beveiligingsproblemen inperken [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "Gebruiken van <q>mktemp</q> in plaats van het verouderde <q>tempfile</q>, waardoor waarschuwingen voorkomen worden">
<correction lemonldap-ng "Verificatieproces repareren in plug-ins voor het testen van wachtwoorden [CVE-2021-40874]">
<correction libarchive "Reparatie voor het uitpakken van harde koppelingen naar symbolische koppelingen; reparatie voor de afhandeling van ACL's van symbolische koppelingen [CVE-2021-23177]; nooit symbolische koppelingen volgen bij het instellen van bestandsvlaggen [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Update van meegeleverde data">
<correction libgdal-grass "Hercompilatie tegen grass 7.8.5-1+deb11u1">
<correction libpod "Bijwerken van de seccomp-ondersteuning om het gebruik van recentere kernelversies mogelijk te maken">
<correction libxml2 "Oplossing voor een probleem van gebruik na vrijgave [CVE-2022-23308]">
<correction linux "Nieuwe bovenstroomse stabiele release; [rt] update naar 5.10.106-rt64; verhogen van ABI naar 13">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; [rt] update naar 5.10.106-rt64; verhogen van ABI naar 13">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; [rt] update naar 5.10.106-rt64; verhogen van ABI naar 13">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; [rt] update naar 5.10.106-rt64; verhogen van ABI naar 13">
<correction mariadb-10.5 "Nieuwe bovenstroomse release; beveiligingsmaatregelen [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "Toevoegen van Breaks: met oudere versies van libmpich1.0-dev om bepaalde opwaarderingsproblemen op te lossen">
<correction mujs "Probleem van bufferoverloop oplossen [CVE-2021-45005]">
<correction mutter "Overzetting van verschillende oplossingen vanuit de bovenstroomse stabiele tak">
<correction node-cached-path-relative "Een probleem met prototypevervuiling oplossen [CVE-2021-23518]">
<correction node-fetch "Geen beveiligde headers doorsturen naar domeinen van derden [CVE-2022-0235]">
<correction node-follow-redirects "Geen Cookie-header versturen tussen domeinen [CVE-2022-0155]; geen vertrouwelijke headers versturen tussen schema's [CVE-2022-0536]">
<correction node-markdown-it "Probleem van denial of service op basis van reguliere expressies oplossen [CVE-2022-21670]">
<correction node-nth-check "Probleem van denial of service op basis van reguliere expressies oplossen [CVE-2021-3803]">
<correction node-prismjs "Opmaak maskeren in commandoregeluitvoer [CVE-2022-23647]; bijwerken van verkleinde bestanden om te garanderen dat een probleem van denial of service met reguliere expressies wordt opgelost [CVE-2021-3801]">
<correction node-trim-newlines "Oplossen van een probleem van denial of service met reguliere expressies [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb: uitschakelen van niet-functionele ondersteuning voor python, wat tot segmentatiefouten leidt; een momentopname van openjdk-8-jre (8u312-b07-1) gebruiken">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse release; oplossing voor een problemen van denial of service [CVE-2022-21813 CVE-2022-21814]; nvidia-kernel-support: aanbieden van /etc/modprobe.d/nvidia-options.conf als sjabloon">
<correction nvidia-modprobe "Nieuwe bovenstroomse release">
<correction openboard "Reparatie van applicatie-icoon">
<correction openssl "Nieuwe bovenstroomse release; reparatie voor armv8-pointerauthenticatie">
<correction openvswitch "Probleem met gebruik-na-vrijgave oplossen [CVE-2021-36980]; herstellen van de installatie van libofproto">
<correction ostree "Compatibiliteit met eCryptFS herstellen; voorkomen van een oneindige recursie bij het herstellen van bepaalde fouten; vastleggingen als partieel markeren voor het downloaden; oplossing voor een assertiefout bij gebruik van een backport of een lokale build van GLib &gt;= 2.71; de mogelijkheid herstellen om OSTree-inhoud op te halen van paden die niet-URI-tekens (zoals backslashes) of niet-ASCII bevatten">
<correction pdb2pqr "Compatibiliteit van propka met Python 3.8 of hoger herstellen">
<correction php-crypt-gpg "Voorkomen dat extra opties worden doorgegeven aan GPG [CVE-2022-24953]">
<correction php-laravel-framework "Probleem met cross-site scripting oplossen [CVE-2021-43808], ontbrekende blokkering van upload van uitvoerbare inhoud [CVE-2021-43617]">
<correction phpliteadmin "Probleem met cross-site scripting oplossen [CVE-2021-46709]">
<correction prips "Oneindige terugloop repareren als een bereik 255.255.255.255 bereikt; CIDR-uitvoer repareren met adressen die verschillen in hun eerste bit">
<correction pypy3 "Reparatie voor compilatiefouten door het verwijderen van een onnodige #endif uit import.h">
<correction python-django "Oplossingen voor een denial of service-probleem [CVE-2021-45115], voor een probleem van informatieontsluiting [CVE-2021-45116], voor een probleem met het doorlopen van mappen [CVE-2021-45452]; reparatie van een traceback rond de afhandeling van RequestSite/get_current_site() vanwege een circulaire import">
<correction python-pip "Vermijden van een race-conditie bij het gebruik van in zip geïmporteerde afhankelijkheden">
<correction rust-cbindgen "Nieuwe bovenstroomse stabiele release om de bouw van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction s390-dasd "Ophouden met het doorgeven van de verouderde optie -f aan dasdfmt">
<correction schleuder "Booleaanse waarden omzetten naar gehele getallen, indien de ActiveRecord verbindingsadapter van SQLite3 in gebruik is, waardoor de functionaliteit wordt hersteld">
<correction sphinx-bootstrap-theme "Zoekfunctionaliteit herstellen">
<correction spip "Verschillende problemen van cross-site scripting oplossen">
<correction symfony "Een probleem van CVE-injectie oplossen [CVE-2021-41270]">
<correction systemd "Ongecontroleerde recursie in systemd-tmpfiles oplossen [CVE-2021-3997]; systemd-timesyncd verlagen van Depends naar Recommends, waardoor een vereistencirkel weggehaald wordt; oplossing voor een fout bij het koppelen van een map met bind mount binnen een container met behulp van machinectl; regressie in udev herstellen die resulteerde in lange vertragingen bij het verwerken van partities met hetzelfde label; een regressie repareren bij gebruik van systemd-networkd in een niet-bevoorrechte LXD-container">
<correction sysvinit "Reparatie voor de verwerking van <q>shutdown +0</q>; verduidelijken dat shutdown niet zal worden afgesloten wanneer het aangeroepen wordt met een <q>tijd</q>">
<correction tasksel "CUPS voor alle *-desktop-taken installeren, omdat task-print-service niet langer bestaat">
<correction usb.ids "Bijwerken van meegeleverde data">
<correction weechat "Oplossing voor een denial of service-probleem [CVE-2021-40516]">
<correction wolfssl "Oplossing voor verschillende problemen in verband met de verwerking van OCSP [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] en de ondersteuning voor TLS1.3 [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "SIGILL-crash op niet-SSE2 CPU's repareren">
<correction xterm "Een probleem van bufferoverloop oplossen [CVE-2022-24130]">
<correction zziplib "Oplossing voor een denial of service-probleem [CVE-2020-18442]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de stabiele
release. Het beveiligingsteam heeft voor elk van deze updates al een advies
uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction angular-maven-plugin "Niet langer bruikbaar">
<correction minify-maven-plugin "Niet langer bruikbaar">

</table>

<h2>Het Debian-installatieprogramma</h2>
<p>Het installatieprogramma werd bijgewerkt om de reparaties die met deze tussenrelease in de stabiele release opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
distributie op &lt;debian-release@lists.debian.org&gt;.</p>



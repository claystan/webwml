#use wml::debian::template title="Installatore Debian (Debian-Installer)" NOHEADER="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6f892e2bb4879338aa7540b6e970f730a9674781" maintainer="Mirco Scottà"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/etch/release.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Notizie</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Notizie precedenti</a>
</p>

<h1>Installare con l'installatore Debian</h1>

<p>
<if-stable-release release="bullseye">
<strong>Per informazioni e per i supporti ufficiali di Debian
<current_release_bullseye></strong>, vedere la
<a href="$(HOME)/releases/bullseye/debian-installer">pagina di bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>Per informazioni e per i supporti ufficiali di Debian
<current_release_bookworm></strong>, vedere la
<a href="$(HOME)/releases/bookworm/debian-installer">pagina di bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Tutti i collegamenti alle immagini riportati qui sotto si riferiscono alla
versione dell'installatore Debian sviluppato per il prossimo rilascio di
Debian e installano la versione Debian <q>testing</q>.
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">

<p>
<strong>Per l'installazione di Debian testing</strong>, suggeriamo l'uso delle
<strong>immagini giornaliere</strong> dell'installatore. Le seguenti immagini
sono quelle giornaliere:
</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">

<p>
<strong>Per installare Debian testing</strong>, suggeriamo l'uso della release
<strong><humanversion /></strong> dell'installatore,
dopo aver controllato l'<a href="errata">errata</a>. Le seguenti immagini sono
quelle disponibili per <humanversion />:
</p>

<h2>Rilascio ufficiale</h2>

<div class="line">
<div class="item col50">
<strong>Immagine dei CD <q>netinst</q> per l'installazione via rete
(circa 180-450&nbsp;MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Insieme completo di CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Insieme completo di DVD</strong>
<full-dvd-images />
</div>


</div>


<div class="line">
<div class="item col50">
<strong>Insieme completo di CD (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Insieme completo di DVD (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<strong>Insieme completo di Blu-ray (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Altre immagini (netboot, chiavetta USB, ecc.)</strong>
<other-images />
</div>
</div>

<p>
Oppure installare <strong>le immagini settimanali di Debian testing</strong>
che usano la stessa versione dell'installatore usata nell'ultimo
rilascio:
</p>

<h2>Immagini settimanali</h2>

<div class="line">
<div class="item col50">
<strong>Insieme completo di CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Insieme completo di DVD</strong>
<devel-full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Insieme completo di CD (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Insieme completo di DVD (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Insieme completo di Blu-ray (tramite <a
href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
Se si cerca qualcosa di più recente per aiutarci a fare il test
dei prossimi rilasci, o per problemi con hardware o altro ancora,
provare una delle <strong>immagini giornaliere</strong> che contengono
l'ultima versione dei componenti dell'installatore.
</p>

</if-testing-installer>

<h2>Immagini giornaliere</h2>

<div class="line">
<div class="item col50">
<strong>Immagini dei CD <q>netinst</q> per l'installazione via rete
(circa 150-280&nbsp;MB)<!-- e businesscard (circa 20-50 MB) --></strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Immagini dei CD <q>netinst</q> per l'installazione via rete
<!-- e businesscard--> (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Immagini dei CD <q>netinst</q> per l'installazione via rete
multi-architettura</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>Altre immagini (netboot, chiavetta usb, ecc.)</strong>
<devel-other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Se sul proprio sistema è presente un qualsiasi hardware che
<strong>richiede il caricamento di firmware non-free</strong> insieme ai
driver del dispositivo, è possibile usare uno dei <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">tarball
con i pacchetti firmware più comuni</a> oppure scaricare
un'immagine <strong>non ufficiale</strong> che contiene i firmware
<strong>non-free</strong>. Le istruzioni su come usare questi tarball e
le informazioni su come caricare il firmware durante l'installazione
possono essere trovate nella <a href="../amd64/ch06s04">guida
all'installazione</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">immagini
non ufficiali con firmware incluso - create quotidianamente</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">immagini
non ufficiali con firmware incluso - create settimanalmente</a>
</p>
</div>

<hr />

<p>
<strong>Note</strong>
</p>
<ul>
#	<li>Prima di scaricare una immagine giornaliera, suggeriamo di
#	dare una scorsa ai <a href="https://wiki.debian.org/DebianInstaller/Today">\
#	problemi conosciuti</a>.</li>
	<li>Una architettura può essere mancante dalla tabella
	delle immagini giornaliere nel caso che queste ultime non siano generate
	in tempo.</li>
	<li>Nella stessa directory delle immagini con l'installatore sono presenti
	dei file di	verifica (<tt>SHA512SUMS</tt> e <tt>SHA256SUMS</tt>)
	delle immagini stesse.</li>
	<li>Per scaricare le immagini CD e DVD complete è consigliato
	l'utilizzo di jigdo.</li>
	<li>Solo un numero limitato di immagini dei set di DVD è
	disponibile come file ISO per lo scaricamento diretto. La maggior parte
	degli utenti non ha bisogno di tutto il software presente in tutti i
	dischi, perciò, per risparmiare spazio sui server e mirror di
	scaricamento, gli insiemi completi sono disponibili solamente tramite
	jigdo.</li>

	<li>L'immagine dei <em>CD netinstall</em> multi architettura supporta
	i386/amd64; l'installazione
	è simile a quella che si esegue con una immagine di rete.</li>
</ul>

<p><strong>Dopo aver usato l'installatore Debian</strong> ci piacerebbe
ricevere un <a
href="https://d-i.debian.org/manual/it.amd64/ch05s04.html#submit-bug">resoconto
(in inglese) dell'installazione</a>, anche se non si fossero verificati
problemi.</p>

<h1>Documentazione</h1>

<p>
<strong>A chi vuole leggere un solo documento</strong> per fare
l'installazione si consiglia l'<a
href="https://d-i.debian.org/manual/it.amd64/apa.html">Installation howto</a>,
una <q>passeggiata</q> di tutto il processo di installazione. Altri documenti
utili sono:
</p>

<ul>
	<li>Manuale d'installazione:
#		<a href="$(HOME)/releases/stable/installmanual">ultima versione
#		rilasciata</a> &mdash;
		<a href="$(HOME)/releases/testing/installmanual">versione in via di riscrittura (testing)</a> &mdash;
		<a href="https://d-i.debian.org/manual/">ultima versione (Git)</a>
		<br />
		istruzioni dettagliate per l'installazione</li>
	<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Domande
		ricorrenti (FAQ) sull'installatore Debian</a> e <a
		href="$(HOME)/CD/faq/">sui CD Debian</a><br />
		domande comuni e relative risposte</li>
	<li><a href="https://wiki.debian.org/DebianInstaller">Wiki
		dell'installatore Debian</a><br />
		documentazione gestita dalla comunit&agrave;
</ul>

<h1>Contatti</h1>

<p>La <a href="https://lists.debian.org/debian-boot/">lista di messaggi
debian-boot</a> è il forum principale di discussione e di lavoro
sull'installatore Debian.
</p>

<p>
C'è anche un canale IRC, #debian-boot, su <tt>irc.debian.org</tt>. Questo
canale è principalmente utilizzato per lo sviluppo, ma occasionalmente per
il supporto.
Se non si riceve alcuna risposta si provi con la lista di messaggi.
</p>

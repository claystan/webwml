#use wml::debian::template title="데비안 10 -- 정오표" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Sebul"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">보안 이슈</toc-add-entry>

<p>데비안 보안 팀은 보안과 관련된 문제를 식별한 안정 릴리스 패키지 업데이트를 발행합니다.
<q>buster</q>에서 확인 된 보안 문제에 대한 정보는 <a href="$(HOME)/security/">보안 페이지</a>를 참조하십시오 .
</p>

<p>APT를 사용한다면, 아래 행을 <tt>/etc/apt/sources.list</tt>에 더해서, 
최근 보안 업데이트를 접근할 수 있게 하세요:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>그 다음, <kbd>apt update</kbd>를 다음과 같이 하세요
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">포인트 릴리스</toc-add-entry>

<p>때로는 몇 가지 중요한 문제나 보안 업데이트인 경우 릴리스된 배포판이 업데이트됩니다. 
일반적으로 이들은 포인트 릴리스로 표시됩니다.
</p>

<ul>
  <li>첫 포인트 릴리스, 10.1을
      <a href="$(HOME)/News/2019/20190907">2019. 9. 7.</a> 릴리스 했습니다.</li>
  <li>두번째 포인트 릴리스, 10.2를 
      <a href="$(HOME)/News/2019/20191116">2019. 11. 16.</a> 릴리스 했습니다.</li>
 <li>세번째 포인트 릴리스, 10.3을 
      <a href="$(HOME)/News/2020/20200208">2020. 2. 8.</a> 릴리스 했습니다.</li>
 <li>네번째 포인트 릴리스, 10.4를 
      <a href="$(HOME)/News/2020/20200509">2020. 5. 9.</a> 릴리스 했습니다.</li>
 <li>다섯번째 포인트 릴리스, 10.5를 
      <a href="$(HOME)/News/2020/20200801">2020. 8. 1.</a> 릴리스 했습니다.</li>
 <li>여섯번째 포인트 릴리스, 10.6을 
      <a href="$(HOME)/News/2020/20200926">2020. 10. 26.</a> 릴리스 했습니다.</li>
 <li>일곱번째 포인트 릴리스, 10.7을 
      <a href="$(HOME)/News/2020/20201205">2020. 12. 5.</a> 릴리스 했습니다.</li>
 <li>여덟번째 포인트 릴리스, 10.8을
      <a href="$(HOME)/News/2021/20210206">2021. 2. 6.</a> 릴리스 했습니다.</li>
 <li>아홉번째 포인트 릴리스, 10.9를
      <a href="$(HOME)/News/2021/20210327">2021. 3. 27.</a> 릴리스 했습니다.</li>            
</ul>

<ifeq <current_release_buster> 10.0 "

<p>There are no point releases for Debian 10 yet.</p>" "

<p><a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
ChangeLog</a>에서 10 및 <current_release_buster/> 변화를 자세히 보세요
</p>"/>


<p>Fixes to the released stable distribution often go through an
extended testing period before they are accepted into the archive.
However, these fixes are available in the
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> directory of any Debian archive
mirror.</p>

<p>If you use APT to update your packages, you can install
the proposed updates by adding the following line to
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 10 point release
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installation system</toc-add-entry>

<p>
For information about errata and updates for the installation system, see
the <a href="debian-installer/">installation information</a> page.
</p>

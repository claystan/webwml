#use wml::debian::template title="用户[CN:支持:][HKTW:支援:]" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9" maintainer="Hsin-lin Cheng"

# $Id$
# Translator: Franklin <franklin@goodhorse.idv.tw>, Fri Nov 15 15:11:02 CST 2002

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">


<ul class="toc">
  <li><a href="#irc">IRC（[CN:实时:][HKTW:即时:][CN:支持:][HKTW:支援:]）</a></li>
  <li><a href="#mail_lists">邮件列表</a></li>
  <li><a href="#usenet">Usenet 新闻[CN:組:][HKTW:群組:]</a></li>
  <li><a href="#forums">Debian 用户论坛</a></li>
  <li><a href="#maintainers">如何联系软件包维护者</a></li>
  <li><a href="#bts">[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統</a></li>
  <li><a href="#release">已知的問題</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 的[CN:支持:][HKTW:支援:]是由一群志愿者提供的。如果[CN:社区:][HKTW:社群:][CN:支持:][HKTW:支援:]不能满足您的需要，并且您也不能在我们的<a href="doc/">[CN:文档:][HKTW:文件:]</a>中找到答案，您可以聘请<a href="consultants/">顾问</a>来解答您的问题，或者维护您的 Debian 系统，或者为其添加新的功能。</p>
</aside>

<h2><a id="irc">IRC（[CN:实时:][HKTW:即时:][CN:支持:][HKTW:支援:]）</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) 提供了一種讓世界各地的人們彼此進行即时交流的途徑。它是一种基于文本的即时通讯系统。使用 IRC 时，您可以加入聊天室（称作频道），也可以直接向特定的人发送私聊信息。
</p>

<p>
您可以在 <a href="https://www.oftc.net/">OFTC</a> 中找到屬於 Debian 的 IRC 頻道。完整的 Debian 频道列表请参见我们的 <a href="https://wiki.debian.org/IRC">Wiki</a>。您也可以使用<a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">搜索引擎</a>查找和 Debian 有关的频道。
</p>

<h3>IRC 客户端</h3>

<p>
要连接到 IRC 网络，您可以在您喜欢的浏览器中打开 OFTC 的<a href="https://www.oftc.net/WebChat/">网页聊天[CN:界面:][HKTW:介面:]</a>，也可以在您的计算机上安装一个客户端。有很多不同的客户端可以使用，有些有图形[CN:界面:][HKTW:介面:]，有些需要在控制台使用。Debian 收录了一些流行的 IRC 客户端软件包，例如：
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a>（文本模式）</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a>（文本模式）</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat（GTK）</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a>（KDE）</li>
</ul> 

<p>
Debian Wiki 上有一个更完整的、已被打包成 Debian 软件包的 <a href="https://wiki.debian.org/IrcClients">IRC 客户端列表</a>。
</p>

<h3>连接到网络</h3>

<p>
當您安裝好客戶端之後，您需要告訴它連到\
哪台[CN:服務器:][HKTW:伺服器:]。對于大多數的客戶端，您只要輸入
</p>

<pre>
/server irc.debian.org
</pre>

<p>主机名 irc.debian.org 是 irc.oftc.net 的别名。对于有些客户端（例如 irssi），则需要用以下的命令代替：</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>加入频道</h3>

<p>當您連上之[CN:后:][HKTW:後:]，請用以下的[CN:命令:][HKTW:指令:]加入 <code>#debian-zh</code> 頻道（中文）：

<pre>
/join #debian-zh
</pre>

<p>如果您想加入英文频道，请使用</p>

<pre>
/join #debian
</pre>

<p>注意：HexChat 或 Konversation 這一類的圖形[CN:界面:][HKTW:介面:] IRC [CN:軟件:][HKTW:軟體:]一般
是通过点击按钮或菜单项来连接到服务器或加入频道。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">阅读我们的 IRC FAQ</a></button></p>

<h2><a id="mail_lists">邮件列表</a></h2>

<p>
来自世界各地的上千名活跃的<a href="intro/people.en.html#devcont">开发者</a>在闲暇时间为 Debian 工作，而他们又来自不同的时区。所以我们主要使用电子邮件进行通信。类似地，Debian 开发者和用户之间的沟通也是在多个不同的<a href="MailingLists/">邮件列表</a>中进行的：
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
  <li>对于中文用户支持，请联系 <a href="https://lists.debian.org/debian-chinese-big5/">debian-chinese-big5 邮件列表</a>（[HK:繁体:][TW:正体:]中文）或 <a href="https://lists.debian.org/debian-chinese-gb/">debian-chinese-gb 邮件列表</a>（简体中文）。此郵件列表歡迎任何中文或英文的文章。兩者不互通，如需完整通知信息请手动进行抄送。请务必使用 UTF-8 编码信件（而非 GB2312/GBK/GB18030/BIG5 等）。</li>
  <li>对于其他语言的用户支持，请查看面向用户的邮件列表的<a href="https://lists.debian.org/users.html">索引</a>。</li>
</ul>

<p>
您无需订阅，即可浏览我们的<a href="https://lists.debian.org/">邮件列表存档</a>或<a href="https://lists.debian.org/search.html">搜索</a>存档。
</p>

<p>
當然還有許多其他不屬於 Debian 的邮件列表，致力於 Linux 生態系統的某些方面。您可使用您最愛的搜尋引擎找到最適合您的。
</p>

<h2><a id="usenet">Usenet 新闻[CN:組:][HKTW:群組:]</a></h2>

<p>
我們許多的<a href="#mail_lists">邮件列表</a>可以在新聞[CN:組:][HKTW:群組:] <kbd>linux.debian.*</kbd> 中找到。
</p>

<h2><a id="forums">Debian 用户论坛</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://forums.debian.net">Debian User Forums</a> 是一個 Debian 系統
的门户网站。有上千的用户在此讨论与 Debian 有关的话题、进行提问、并回答
他人的问题。阅读所有的版面都不需要注册。如果您想参与讨论并发表帖子，请
注册并登录。
</p>

<h2><a id="maintainers">如何联系软件包维护者</a></h2>

<p>
基本上，有两种常用的方式可以联系 Debian 软件包的维护者：
</p>

<ul>
  <li>如果您想报告[CN:缺陷:][HKTW:錯誤:]，只需要提交<a href="bts">[CN:缺陷:][HKTW:錯誤:]报告</a>就可以了；维护者会自动收到您的[CN:缺陷:][HKTW:錯誤:]报告的一个副本。</li>
  <li>如果您只是想给维护者发送电子邮件，请使用每个软件包的特殊的邮件地址别名：<br>
      &lt;<em>软件包名</em>&gt;@packages.debian.org</li>
</ul>

<h2><a id="bts">[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統</a></h2>

<p>
Debian 发行版有自己的<a href="Bugs/">[CN:缺陷跟蹤:][HKTW:錯誤追蹤:]系統</a>，包含了用户和开发者报告的[CN:缺陷:][HKTW:錯誤:]。 每個[CN:軟件缺陷報告:][HKTW:軟體錯誤報告:]都被授予一個編號並且被長期[CN:跟蹤:][HKTW:追蹤:]，直到它被標記為已修復。有两种方式可以报告[CN:缺陷:][HKTW:錯誤:]：
</p>

<ul>
  <li>推荐的方式是使用 Debian 软件包 <em>reportbug</em>。</li>
  <li>或者，您可以按照这个<a href="Bugs/Reporting">页面</a>中描述的方法发送电子邮件。</li>
</ul>

<h2><a id="release">已知的問題</a></h2>

<p>您可以在<a href="releases/stable/">[CN:发布:][HKTW:释出:]页面</a>中找到和現在的穩定版本\
相關的技術限制和重大問題（如果有的话）。</p>

<p>請特別留意<a href="releases/stable/releasenotes">發行\
说明</a>和<a href="releases/stable/errata">錯誤修正</a>中的訊息。</p>

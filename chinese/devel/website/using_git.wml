#use wml::debian::template title="使用 git 进行 Debian 网站开发" MAINPAGE="true"
#use wml::debian::translation-check translation="af1ccd5ffd8c21da67f3117154ef989f582609b5"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">使用 Git 存储库开展工作</a></li>
<li><a href="#write-access">Git 存储库写入权限</a></li>
<li><a href="#notifications">接收通知</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a> \
是一个<a href="https://en.wikipedia.org/wiki/Version_control">版本控制系统</a>\
，有助于多个开发者的协同工作。每个用户都可以拥有主存储库的本地副本。本地副本可以\
位于同一台计算机上，也可以位于世界各地。开发者可以修改本地副本，并在完成\
资料的修改后，提交更改并将其推回主存储库。</p>
</aside>

<h2><a id="work-on-repository">使用 Git 存储库开展工作</a></h2>

<p>
让我们直奔主题——本节中您将了解如何创建主存储库的本地副本、如何使本地副本\
保持最新，以及如何提交您的工作。我们也会介绍如何开展翻译工作。
</p>

<h3><a name="get-local-repo-copy">获取一份本地副本</a></h3>

<p>
首先，安装 Git。然后，配置 Git 并输入您的姓名和电子邮件地址。\
如果您是 Git 的新用户，先阅读通用的 Git 文档可能是个好主意。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Git 文档</a></button></p>

<p>
下一步是克隆存储库（换句话说，对其创建本地副本）。\
有两种方法：
</p>

<ul>
  <li>在 <url https://salsa.debian.org/> 上注册一个帐户，并通过将 SSH 公钥上传到您的 salsa 帐户\
  来启用 SSH 访问。更多信息，请参见 <a
  href="https://salsa.debian.org/help/ssh/index.md">Salsa 帮助页面</a>。然后，您可以使用以下命令\
  克隆 <code>webwml</code> 存储库：
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>另一种方式是，您可以使用 HTTPS 协议克隆存储库。请注意，虽然这会创建一个\
  本地副本，但是您将无法直接以这种方式将修改直接推送回远程仓库：
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>提示：</strong>克隆整个 <code>webwml</code> 存储库需要下载\
大约 1.3 GB 的数据，如果您的网络连接太慢或不稳定，这个数据量就太大了。\
所以，可以先以最小深度进行克隆，以减少首次下载的数据量：
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>获得可用的（浅）存储库后，您可以加深本地副本，并最终将其转换为完整的\
本地存储库：</p>

<pre>
  git fetch --deepen=1000 # 为仓库加深 1000 次提交
  git fetch --unshallow   # 获取所有未获取的提交，将仓库转换成完整的
</pre>

<p>您也可以仅检出部分页面，如下所示：</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>在 <code>webwml</code> 中创建文件 <code>.git/info/sparse-checkout</code>，\
  定义您想要检出的内容。例如，如果您只想要基本文件、英语、加泰罗尼亚语和西班牙语\
  翻译，文件内容应该像下面这样：
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>最后，您就可以检出库：<code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">提交本地修改</a></h3>

<h4><a name="keep-local-repo-up-to-date">令您的本地仓库保持最新</a></h4>

<p>每隔几天（也一定要在开始一些编辑工作之前！），您应该运行一次</p>

<pre>
  git pull
</pre>

<p>以从存储库中获取已被更改的文件。</p>

<p>
强烈建议在执行 <code>git pull</code> 和开始编辑文件之前保持本地 Git 工作目录的\
干净。如果您有未提交的更改，或者当前分支的远程存储库中不存在的本地提交，则\
执行 <code>git pull</code> 将会自动创建合并提交，甚至会由于冲突而导致拉取失败。\
请考虑将未完成的工作保存在另一个分支中，或使用 <code>git stash</code> 之类的命令。
</p>

<p>注意：Git 是一个分布式（而非集中式）版本控制系统。这意味着当您提交更改时，\
它们将仅存储在本地存储库中。要与他人共享它们，您还需要将所做的更改推送\
到 Salsa 上的中央存储库。</p>

<h4><a name="example-edit-english-file">示例：编辑文件</a></h4>

<p>
让我们看一个更实际的例子，也就是典型的编辑过程。我们假设您已经
使用 <code>git clone</code> 获得了库的一份<a href="#get-local-repo-copy">本地副本</a>。\
接下来的步骤是：
</p>

<ol>
  <li><code>git pull</code></li>
  <li>现在您可以开始编辑并对文件进行修改。</li>
  <li>当您完成后，使用以下命令将您的更改提交到本地存储库：
    <pre>
    git add /path/to/file(s)
    git commit -m "Your commit message"
    </pre></li>
  <li>如果您有对远程 <code>webwml</code> 存储库的<a
  href="#write-access">不受限制的写入权限</a>，那么现在可以将您所做的更改直接\
  推送到 Salsa 存储库中：<code>git push</code></li>
  <li>如果您无权直接写入 <code>webwml</code> 存储库，请考虑使用<a
  href="#write-access-via-merge-request">合并请求</a>提交您的更改，或寻求其他开发人员的帮助。</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Git 文档</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">在 Git 提交中关闭 Debian 的缺陷</a></h4>

<p>
如果您的提交日志条目中包含 <code>Closes: #</code><var>nnnnnn</var>，那么在您推送\
您的更改后，缺陷编号 <code>#</code><var>nnnnnn</var> 将会被自动关闭。精确的格式与 \
<a href="$(DOC)/debian-policy/ch-source.html#id24">Debian 政策中的介绍</a>相同。</p>

<h4><a name="links-using-http-https">使用 HTTP/HTTPS 链接</a></h4>

<p>许多 Debian 网站都支持 SSL/TLS，因此请尽可能使用 HTTPS 链接。<strong>但是</strong>，\
某些 Debian/DebConf/SPI 等网站要么不具有 HTTPS 支持，要么仅使用 SPI 数字证书认证机构\
（并不是一个被所有浏览器信任的 SSL 证书认证机构）。为了避免给非 Debian 用户带来错误消息\
的困扰，请不要使用 HTTPS 链接到此类站点。</p>

<p>Git 存储库会拒绝那些包含使用纯 HTTP 链接到支持 HTTPS 的 Debian 网站的内容或包含使用 HTTPS \
链接到已知不支持 HTTPS 或仅使用 SPI 签名的证书的 Debian/DebConf/SPI 网站的内容的提交。</p>

<h3><a name="translation-work">开展翻译工作</a></h3>

<p>翻译应始终与相对应的英文文件同步并保持最新。翻译文件中\
的 <code>translation-check</code> 头用于跟踪当前翻译所基于的英语文件的版本。\
如果您更改了已翻译的文件，则需要更新 <code>translation-check</code> 头以匹配\
英语文件中相应更改的 Git 提交哈希值。您可以使用以下命令找到该哈希值：</p>

<pre>
  git log path/to/english/file
</pre>

<p>如果您要对一个文件新建翻译，请使用 <code>copypage.pl</code> 脚本，它将为您的语言创建一个\
模板，该模板包含了正确的翻译头。</p>

<h4><a name="translation-smart-change">使用 smart_change.pl 进行翻译修改</a></h4>

<p><code>smart_change.pl</code> 是一个脚本，目的是让同时更新原始文件及其翻译更为容易。\
有两种方法供您使用，选择哪种取决于您进行的更改。</p>

<p>
以下步骤说明了如何使用 <code>smart_change.pl</code>，以及当手动修改文件时，如何只\
更新 <code>translation-check</code> 头：
</p>

<ol>
  <li>对原始文件进行更改，然后提交。</li>
  <li>更新翻译。</li>
  <li>运行 <code>smart_change.pl -c COMMIT_HASH</code>（使用对原始文件的更改产生\
  的提交哈希值）。它会获取该更改，并更新已翻译文件的头。</li>
  <li>检查更改（例如，使用 <code>git diff</code>）。</li>
  <li>提交翻译更改。</li>
</ol>

<p>
或者，您可以使用正则表达式来一次性对文件进行多个更改：
</p>

<ol>
  <li>运行 <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>检查更改（例如，使用 <code>git diff</code>）。</li>
  <li>提交原始文件。</li>
  <li>运行 <code>smart_change.pl origfile1 origfile2</code>（即，\
  这次<strong>不带正则表达式</strong>）。它就会只更新已翻译文件的头。</li>
  <li>最后，提交翻译更改。</li>
</ol>

<p>
必须承认，这比第一个例子更麻烦一点，因为涉及到两次提交，但是，由于 Git 哈希值的\
工作方式，这是无法避免的。
</p>

<h2><a id="write-access">Git 存储库写入权限</a></h2>

<p>
整个 Debian 网站源代码都使用 Git 管理。它位于 <url https://salsa.debian.org/webmaster-team/webwml/>。\
默认情况下，不允许访客推送自己的提交到源代码存储库中。如果您想要对 Debian 网站\
作出贡献，您需要一些许可才能获得对存储库的写入权限。
</p>

<h3><a name="write-access-unlimited">不受限制的写入权限</a></h3>

<p>
如果您需要对存储库的不受限制的写入权限（例如，您打算成为频繁提交的贡献者），请在\
登录到 Debian Salsa 平台后通过 <url https://salsa.debian.org/webmaster-team/webwml/> \
网页界面请求写入权限。
</p>

<p>
如果您刚开始参与 Debian 网站开发，并且没有经验，请在请求不受限制的写入权限\
之前发送电子邮件到 <a
href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a> \
介绍一下您自己。请您考虑在自我介绍中提供更多有用的信息，例如您\
打算对网站的哪个部分进行修改、您说哪种语言，以及是否有 Debian 团队的\
其他成员可以为您担保。
</p>

<h3><a name="write-access-via-merge-request">合并请求（Merge Request）</a></h3>

<p>
获得对存储库的不受限制的写入权限并不是必需的——您可以随时提交合并请求，\
并让其他开发人员检查并接受您的成果。请使用由 Salsa GitLab 平台通过其网页界面提供的标准程序提交\
合并请求，并请阅读以下两篇文档：
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a></li>
</ul>

<p>
注意，并不是所有的网站开发人员都随时查看合并请求，所以您可能需要等一段时间才能收到反馈。如果您不确定所作的\
贡献是否会被接受，请发送电子邮件至 <a href="https://lists.debian.org/debian-www/">debian-www</a> \
邮件列表请求核查。
</p>

<h2><a id="notifications">接收通知</a></h2>

<p>
如果您正在 Debian 网站上开展工作，您可能想要知道 <code>webwml</code> 存储库的动向。有两种方法可以让您始终处于圈子内：提交通知和合并请求通知。</p>

<h3><a name="commit-notifications">接收提交通知</a></h3>

<p>我们已经在 Salsa 中配置好了 <code>webwml</code> 项目，提交会显示在 IRC 频道 #debian-www 中。</p>

<p>
如果您想要在 <code>webwml</code> 存储库中有提交时通过电子邮件接收通知，请按照以下步骤\
通过 <a href="https://tracker.debian.org/">tracker.debian.org</a> 订阅 <code>www.debian.org</code> 伪软件包并在其中激活 <code>vcs</code> \
关键字（仅需一次）：</p>

<ol>
  <li>打开网页浏览器并访问 <url https://tracker.debian.org/pkg/www.debian.org>。</li>
  <li>订阅 <code>www.debian.org</code> 伪软件包（如果您尚未在其它情况下使用过  tracker.debian.org\
      ，则可以使用 SSO 通过验证或使用电子邮件和密码进行注册）。</li>
  <li>跳转到 <url https://tracker.debian.org/accounts/subscriptions/>，然后点击 <code>modify keywords</code>\
      ，选中 <code>vcs</code>（如果其未被选中）并保存。</li>
  <li>从现在开始，每当有人提交更改到 <code>webwml</code> 存储库时，您将收到电子邮件。</li>
</ol>

<h3><a name="merge-request-notifications">接收合并请求通知</a></h3>

<p>
如果您想要在 Salsa 上的 <code>webwml</code> 存储库收到新的合并请求（Merge Request）\
的时候得到电子邮件提醒，您可以在网页界面上配置您的通知设置。步骤如下：
</p>

<ol>
  <li>登陆您的 Salsa 帐号并前往项目页面。</li>
  <li>点击项目主页顶部的铃铛图标。</li>
  <li>选择您所需的通知级别。</li>
</ol>

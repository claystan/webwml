<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in libxslt the XSLT 1.0 processing
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4610">CVE-2016-4610</a>

    <p>Invalid memory access leading to DoS at exsltDynMapFunction. libxslt
    allows remote attackers to cause a denial of service (memory
    corruption) or possibly have unspecified other impact via unknown
    vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4609">CVE-2016-4609</a>

    <p>Out-of-bounds read at xmlGetLineNoInternal()
    libxslt allows remote attackers to cause a denial of service (memory
    corruption) or possibly have unspecified other impact via unknown
    vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13117">CVE-2019-13117</a>

    <p>An xsl:number with certain format strings could lead to an
    uninitialized read in xsltNumberFormatInsertNumbers. This could
    allow an attacker to discern whether a byte on the stack contains
    the characters A, a, I, i, or 0, or any other character.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13118">CVE-2019-13118</a>

    <p>A type holding grouping characters of an xsl:number instruction was
    too narrow and an invalid character/length combination could be
    passed to xsltNumberFormatDecimal, leading to a read of
    uninitialized stack data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.1.28-2+deb8u5.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1860.data"
# $Id: $

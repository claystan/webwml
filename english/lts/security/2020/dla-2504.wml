<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in MediaWiki, a website engine
for collaborative work.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15005">CVE-2020-15005</a>

    <p>Private wikis behind a caching server using the img_auth.php image
    authorization security feature may have had their files cached
    publicly, so any unauthorized user could view them.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35477">CVE-2020-35477</a>

    <p>Blocks legitimate attempts to hide log entries in some situations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35479">CVE-2020-35479</a>

    <p>Allows XSS via BlockLogFormatter.php. Language::translateBlockExpiry
    itself does not escape in all code paths. For example, the return of
    Language::userTimeAndDate is is always unsafe for HTML in a month
    value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35480">CVE-2020-35480</a>

    <p>Missing users (accounts that don't exist) and hidden users (accounts
    that have been explicitly hidden due to being abusive, or similar)
    that the viewer cannot see are handled differently, exposing
    sensitive information about the hidden status to unprivileged
    viewers.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:1.27.7-1~deb9u7.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2504.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The json-jwt gem before 1.11.0 for Ruby lacks an element
count during the splitting of a JWE string.</p>

<p>Therefore, there was a need to explicitly specify the number
of elements when splitting a JWE string.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.6.2-1+deb9u2.</p>

<p>We recommend that you upgrade your ruby-json-jwt packages.</p>

<p>For the detailed security status of ruby-json-jwt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-json-jwt">https://security-tracker.debian.org/tracker/ruby-json-jwt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2390.data"
# $Id: $

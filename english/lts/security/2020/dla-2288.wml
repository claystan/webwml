<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following CVE(s) were reported against src:qemu:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9503">CVE-2017-9503</a>

    <p>QEMU (aka Quick Emulator), when built with MegaRAID SAS 8708EM2
    Host Bus Adapter emulation support, allows local guest OS
    privileged users to cause a denial of service (NULL pointer
    dereference and QEMU process crash) via vectors involving megasas
    command processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12068">CVE-2019-12068</a>

    <p>In QEMU 1:4.1-1 (1:2.8+dfsg-6+deb9u8), when executing script in
    lsi_execute_script(), the LSI scsi adapter emulator advances
    's->dsp' index to read next opcode. This can lead to an infinite
    loop if the next opcode is empty. Move the existing loop exit
    after 10k iterations so that it covers no-op opcodes as well.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20382">CVE-2019-20382</a>

    <p>QEMU 4.1.0 has a memory leak in zrle_compress_data in
    ui/vnc-enc-zrle.c during a VNC disconnect operation because libz
    is misused, resulting in a situation where memory allocated in
    deflateInit2 is not freed in deflateEnd.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1983">CVE-2020-1983</a>

    <p>A use after free vulnerability in ip_reass() in ip_input.c of
    libslirp 4.2.0 and prior releases allows crafted packets to cause
    a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

    <p>In libslirp 4.1.0, as used in QEMU 4.2.0, tcp_subr.c misuses
    snprintf return values, leading to a buffer overflow in later
    code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10756">CVE-2020-10756</a>

    <p>An out-of-bounds read vulnerability was found in the SLiRP
    networking implementation of the QEMU emulator. This flaw occurs
    in the icmp6_send_echoreply() routine while replying to an ICMP
    echo request, also known as ping. This flaw allows a malicious
    guest to leak the contents of the host memory, resulting in
    possible information disclosure. This flaw affects versions of
    libslirp before 4.3.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13361">CVE-2020-13361</a>

    <p>In QEMU 5.0.0 and earlier, es1370_transfer_audio in
    hw/audio/es1370.c does not properly validate the frame count,
    which allows guest OS users to trigger an out-of-bounds access
    during an es1370_write() operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13362">CVE-2020-13362</a>

    <p>In QEMU 5.0.0 and earlier, megasas_lookup_frame in
    hw/scsi/megasas.c has an out-of-bounds read via a crafted
    reply_queue_head field from a guest OS user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13659">CVE-2020-13659</a>

    <p>address_space_map in exec.c in QEMU 4.2.0 can trigger a NULL
    pointer dereference related to BounceBuffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13754">CVE-2020-13754</a>

    <p>hw/pci/msix.c in QEMU 4.2.0 allows guest OS users to trigger
    an out-of-bounds access via a crafted address in an msi-x mmio
    operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13765">CVE-2020-13765</a>

    <p>rom_copy() in hw/core/loader.c in QEMU 4.1.0 does not validate
    the relationship between two addresses, which allows attackers
    to trigger an invalid memory copy operation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

    <p>Stack-based overflow in xgmac_enet_send() in hw/net/xgmac.c.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u10.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2288.data"
# $Id: $

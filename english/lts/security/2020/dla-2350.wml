<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Horde Groupware, there has been an XSS via the Name field during
creation of a new Resource. This could have been leveraged for remote
code execution after compromising an administrator account, because the
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7984">CVE-2015-7984</a> CSRF protection mechanism can then be bypassed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.2.19-1+deb9u1.</p>

<p>We recommend that you upgrade your php-horde-kronolith packages.</p>

<p>For the detailed security status of php-horde-kronolith please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-horde-kronolith">https://security-tracker.debian.org/tracker/php-horde-kronolith</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2350.data"
# $Id: $

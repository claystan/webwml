<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that ZeroMQ, a lightweight messaging kernel
library does not properly handle connecting peers before a
handshake is completed. A remote, unauthenticated client connecting
to an application using the libzmq library, running with a socket
listening with CURVE encryption/authentication enabled can take
advantage of this flaw to cause a denial of service affecting
authenticated and encrypted clients.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.2.1-4+deb9u3.</p>

<p>We recommend that you upgrade your zeromq3 packages.</p>

<p>For the detailed security status of zeromq3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zeromq3">https://security-tracker.debian.org/tracker/zeromq3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2443.data"
# $Id: $

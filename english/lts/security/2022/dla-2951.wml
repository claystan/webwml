<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In append_to_verify_fifo_interleaved_ of stream_encoder.c, there is
a possible out of bounds write due to a missing bounds check. This
could lead to local information disclosure with no additional
execution privileges needed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.3.2-2+deb9u2.</p>

<p>We recommend that you upgrade your flac packages.</p>

<p>For the detailed security status of flac please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/flac">https://security-tracker.debian.org/tracker/flac</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2951.data"
# $Id: $

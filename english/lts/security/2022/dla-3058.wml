<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in libsndfile, a library for reading/writing
audio files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12562">CVE-2017-12562</a>

    <p>Due to a possible heap buffer overflow attack in an attacker could
    cause a remote denial of service attack by tricking the function into
    outputting a largeamount of data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4156">CVE-2021-4156</a>

    <p>Using a crafted FLAC file, an attacker could trigger an out-of-bounds
    read that would most likely cause a crash but could potentially leak
    memory information.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.0.27-3+deb9u3.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>For the detailed security status of libsndfile please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsndfile">https://security-tracker.debian.org/tracker/libsndfile</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3058.data"
# $Id: $

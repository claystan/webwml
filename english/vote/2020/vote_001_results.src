           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Graphical rendering of the results">
               </a>
           </p>
             <p>
               In the graph above, any pink colored nodes imply that
               the option did not pass majority, the Blue is the
               winner. The Octagon is used for the options that did
               not beat the default.  
           </p>
           <ul>
<li>Option 1 "Jonathan Carter"</li>
<li>Option 2 "Sruthi Chandran"</li>
<li>Option 3 "Brian Gupta"</li>
<li>Option 4 "None Of The Above"</li>
           </ul>
            <p>
               In the following table, tally[row x][col y] represents
               the votes that option x received over option y. A 
               <a href="https://en.wikipedia.org/wiki/Schwartz_method">more
                 detailed explanation of the beat matrix</a> may help in
               understanding the table. For understanding the Condorcet method, the
               <a href="https://en.wikipedia.org/wiki/Condorcet_method">Wikipedia
                 entry</a> is fairly informative.
           </p>
           <table class="vote">
             <caption class="center"><strong>The Beat Matrix</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="4" class="center">Option</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
                   <th>    4 </th>
              </tr>
                 <tr>
                   <th>Option 1  </th>
                   <td>&nbsp;</td>
                   <td>  258 </td>
                   <td>  281 </td>
                   <td>  301 </td>
                 </tr>
                 <tr>
                   <th>Option 2  </th>
                   <td>   57 </td>
                   <td>&nbsp;</td>
                   <td>  163 </td>
                   <td>  244 </td>
                 </tr>
                 <tr>
                   <th>Option 3  </th>
                   <td>   40 </td>
                   <td>  114 </td>
                   <td>&nbsp;</td>
                   <td>  194 </td>
                 </tr>
                 <tr>
                   <th>Option 4  </th>
                   <td>   34 </td>
                   <td>   76 </td>
                   <td>  125 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

Looking at row 2, column 1, Sruthi Chandran<br/>
received 57 votes over Jonathan Carter<br/>
<br/>
Looking at row 1, column 2, Jonathan Carter<br/>
received 258 votes over Sruthi Chandran.<br/>
              <h3>Pair-wise defeats</h3>
              <ul>
                <li>Option 1 defeats Option 2 by ( 258 -   57) =  201 votes.</li>
                <li>Option 1 defeats Option 3 by ( 281 -   40) =  241 votes.</li>
                <li>Option 1 defeats Option 4 by ( 301 -   34) =  267 votes.</li>
                <li>Option 2 defeats Option 3 by ( 163 -  114) =   49 votes.</li>
                <li>Option 2 defeats Option 4 by ( 244 -   76) =  168 votes.</li>
                <li>Option 3 defeats Option 4 by ( 194 -  125) =   69 votes.</li>
              </ul>
              <h3>The Schwartz Set contains</h3>
              <ul>
                <li>Option 1 "Jonathan Carter"</li>
              </ul>
              <h3>The winners</h3>
              <ul>
                <li>Option 1 "Jonathan Carter"</li>
              </ul>
              <p>
               Debian uses the Condorcet method for voting.
               Simplistically, plain Condorcets method
               can be stated like so : <br/>
               <q>Consider all possible two-way races between candidates.
                  The Condorcet winner, if there is one, is the one
                  candidate who can beat each other candidate in a two-way
                  race with that candidate.</q>
               The problem is that in complex elections, there may well
               be a circular relationship in which A beats B, B beats C,
               and C beats A. Most of the variations on Condorcet use
               various means of resolving the tie. See
               <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">Cloneproof Schwartz Sequential Dropping</a>
               for details. Debian's variation is spelled out in the
               <a href="$(HOME)/devel/constitution">constitution</a>,
               specifically,  A.6.
              </p>

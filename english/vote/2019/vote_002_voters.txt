-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4           adamm	Adam Majer
    5         adejong	Arthur de Jong
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8             agi	Alberto Gonzalez Iniesta
    9             agx	Guido Guenther
   10              ah	Andreas Henriksson
   11         alessio	Alessio Treglia
   12           alexm	Alex Muntada
   13           alexp	Alex Pennace
   14        alteholz	Thorsten Alteholz
   15           amaya	Amaya Rodrigo Sastre
   16        ametzler	Andreas Metzler
   17             ana	Ana Beatriz Guerrero López
   18         anarcat	Antoine Beaupré
   19            anbe	Andreas Beckmann
   20            andi	Andreas B. Mundt
   21        andrewsh	Andrej Shadura
   22        angdraug	Dmitry Borodaenko
   23           angel	Angel Abad
   24          ansgar	Ansgar Burchardt
   25         antonio	Antonio Radici
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26             apo	Markus Koschany
   27         apoikos	Apollon Oikonomopoulos
   28            arno	Arno Töll
   29            aron	Aron Xu
   30          arturo	Arturo Borrero González
   31         aurel32	Aurelien Jarno
   32           aviau	Alexandre Viau
   33             awm	Andrew McMillan
   34       awoodland	Alan Woodland
   35              az	Alexander Zangerl
   36        azekulic	Alen Zekulic
   37        ballombe	Bill Allombert
   38             bas	Bas Zoetekouw
   39          bbaren	Benjamin Barenblat
   40           bdale	Bdale Garbee
   41          bengen	Hilko Bengen
   42            benh	Ben Hutchings
   43          bernat	Vincent Bernat
   44           berni	Bernhard Schmidt
   45           berto	Alberto Garcia
   46            beuc	Sylvain Beucler
   47         bgoglin	Brice Goglin
   48          bgupta	Brian Gupta
   49           biebl	Michael Biebl
   50         bigeasy	Sebastian Andrzej Siewior
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51           bigon	Laurent Bigonville
   52           blade	Eduard Bloch
   53           bluca	Luca Boccassi
   54             bod	Brendan O'Dea
   55           bootc	Chris Boot
   56         bottoms	Maitland Bottoms
   57         bremner	David Bremner
   58         broonie	Mark Brown
   59         bureado	Jose Parrella
   60           byang	Boyuan Yang
   61            bzed	Bernd Zeimetz
   62        calculus	Jerome Georges Benoit
   63        capriott	Andrea Capriotti
   64          carnil	Salvatore Bonaccorso
   65             cas	Craig Sanders
   66        cascardo	Thadeu Cascardo
   67          cbiedl	Christoph Biedl
   68            cech	Petr Cech
   69          chrism	Christoph Martin
   70       christoph	Christoph Egger
   71       chronitis	Gordon Ball
   72        cjwatson	Colin Watson
   73             ckk	Christian Kastner
   74           cklin	Chuan-kai Lin
   75           clint	Clint Adams
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76        codehelp	Neil Williams
   77            cord	Cord Beermann
   78         coucouf	Aurélien Couderc
   79             cts	Christian T. Steigies
   80           cwryu	Changwoo Ryu
   81          czchen	ChangZhuo Chen
   82             dai	Daisuke Higuchi
   83          daniel	Daniel Baumann
   84           dapal	David Paleino
   85            dato	Dato Simó
   86         debacle	Wolfgang Borgert
   87       directhex	Jo Shields
   88          dirson	Yann Dirson
   89          dkogan	Dima Kogan
   90       dktrkranz	Luca Falavigna
   91          dlange	Daniel Lange
   92           dlehn	David I. Lehn
   93             dmn	Damyan Ivanov
   94             dod	Dominique Dumont
   95         dogsleg	Lev Lamberov
   96         donkult	David Kalnischkies
   97       dottedmag	Mikhail Gusarov
   98            duck	Marc Dequènes
   99          ebourg	Emmanuel Bourg
  100             edd	Dirk Eddelbuettel
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101         edmonds	Robert Edmonds
  102        ehashman	Elana Hashman
  103          elbrus	Paul Mathijs Gevers
  104            eloy	Krzysztof Krzyzaniak
  105             ema	Emanuele Rocca
  106          enrico	Enrico Zini
  107        eriberto	Joao Eriberto Mota Filho
  108            eric	Eric Dorland
  109           erich	Erich Schubert
  110           eriks	Erik Schanze
  111           eriol	Daniele Tricoli
  112           eugen	Eugeniy Meshcheryakov
  113          eugene	Eugene Zhukov
  114           fabbe	Fabian Fagerholm
  115          fabian	Fabian Greffrath
  116             faw	Felipe Augusto van de Wiel
  117          fgeyer	Felix Geyer
  118         filippo	Filippo Giunchedi
  119         florian	Florian Ernst
  120         fpeters	Frederic Peters
  121        francois	Francois Marier
  122         frankie	Francesco Lovergine
  123        fsateler	Felipe Sateler
  124            fsfs	Florian Schlichting
  125           fuddl	Bruno Kleinert
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126              fw	Florian Weimer
  127         genannt	Jonas Genannt
  128           georg	Georg Faerber
  129        georgesk	Georges Khaznadar
  130            gewo	Gert Wollny
  131           ghedo	Alessandro Ghedini
  132          ginggs	Graham Inggs
  133             gio	Giovanni Mascellani
  134         giovani	Giovani Augusto Ferreira
  135           gladk	Anton Gladky
  136        glaubitz	John Paul Adrian Glaubitz
  137          glondu	Stéphane Glondu
  138          gniibe	NIIBE Yutaka
  139         godisch	Martin A. Godisch
  140          gregoa	Gregor Herrmann
  141         guilhem	Guilhem Moulin
  142         guillem	Guillem Jover
  143        guoliang	Liang Guo
  144          gusnan	Andreas Rönnquist
  145            guus	Guus Sliepen
  146           gwolf	Gunnar Wolf
  147            haas	Christoph Haas
  148        hartmans	Sam Hartman
  149           hefee	Sandro Knauß
  150         helmutg	Helmut Grohne
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151         henrich	Hideki Yamane
  152         hertzog	Raphaël Hertzog
  153      hlieberman	Harlan Lieberman-Berg
  154             hmh	Henrique de Moraes Holschuh
  155         hoexter	Sven Hoexter
  156          holger	Holger Levsen
  157          hsteoh	Hwei Sheng Teoh
  158             hug	Philipp Hug
  159            ianw	Ian Wienand
  160             ijc	Ian James Campbell
  161       intrigeri	Intrigeri
  162        ishikawa	Ishikawa Mutsumi
  163          iustin	Iustin Pop
  164           ivodd	Ivo De Decker
  165             iwj	Ian Jackson
  166          jackyf	Eugene V. Lyubimkin
  167             jak	Julian Andres Klode
  168         jaldhar	Jaldhar H. Vyas
  169           jandd	Jan Dittberner
  170         jbfavre	Jean Baptiste Favre
  171          jbicha	Jeremy Bicha
  172             jcc	Jonathan Cristopher Carter
  173        jcowgill	James Cowgill
  174        jcristau	Julien Cristau
  175          jelmer	Jelmer Vernooij
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176             jfs	Javier Fernandez-Sanguino Peña
  177        jgoerzen	John Goerzen
  178              jh	Jörgen Hägg
  179             jjr	Jeffrey Ratcliffe
  180             jmm	Moritz Muehlenhoff
  181            jmtd	Jonathan Dowland
  182           joerg	Joerg Jaspert
  183           johns	John Sullivan
  184         joostvb	Joost van Baal
  185           jordi	Jordi Mallach
  186           josch	Johannes Schauer
  187           josue	Josué Ortega
  188         joussen	Mario Joussen
  189          jpuydt	Julien Puydt
  190          jrtc27	James Clarke
  191              js	Jonas Smedegaard
  192        jspricke	Jochen Sprickerhof
  193            jule	Juliane Friederike Holzt
  194         kaction	Dmitry Bogatov
  195           kamop	Atsushi Kamoshida
  196         kapouer	Jérémy Lal
  197          kartik	Kartik Mistry
  198             kcr	Karl Ramm
  199          keithp	Keith Packard
  200          khalid	Khalid Aziz
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201            kibi	Cyril Brulebois
  202        kilobyte	Adam Borowski
  203       kitterman	Scott Kitterman
  204            knok	Takatsugu Nokubi
  205           kobla	Ondřej Kobližek
  206           krait	Christopher Knadle
  207           krala	Antonin Kral
  208      kritzefitz	Sven Bartscher
  209            kula	Marcin Kulisz
  210           lamby	Chris Lamb
  211           laney	Iain Lane
  212           lange	Thomas Lange
  213         larjona	Laura Arjona Reina
  214        lavamind	Jerome Charaoui
  215        lawrencc	Christopher Lawrence
  216      lazyfrosch	Markus Frosch
  217         legoktm	Kunal Mehta
  218             leo	Carsten Leonhardt
  219        lfaraone	Luke Faraone
  220        lfilipoz	Luca Filipozzi
  221        lightsey	John Lightsey
  222           lindi	Timo Juhani Lindfors
  223         lingnau	Anselm Lingnau
  224          lintux	Wilmer van der Gaast
  225          lkajan	Laszlo Kajan
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226   locutusofborg	Gianfranco Costamagna
  227          lohner	Nils Boeffel
  228           lucab	Luca Bruno
  229           lucas	Lucas Nussbaum
  230           lumin	Mo Zhou
  231         madduck	Martin F. Krafft
  232            mafm	Manuel A. Fernandez Montecelo
  233             mak	Matthias Klumpp
  234           mattb	Matthew Brown
  235         matthew	Matthew Vernon
  236     matthieucan	Matthieu Caneill
  237          mattia	Mattia Rizzolo
  238          mbanck	Michael Banck
  239         mbehrle	Mathias Behrle
  240              md	Marco d'Itri
  241           mehdi	Mehdi Dogguy
  242            mejo	Jonas Meurer
  243          merker	Karsten Merker
  244          meskes	Michael Meskes
  245           metal	Marcelo Jorge Vieira
  246             mfv	Matteo F. Vescovi
  247        mgilbert	Michael Gilbert
  248          mhatta	Masayuki Hatta
  249             mhy	Mark Hymers
  250           micah	Micah Anderson
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251            mika	Michael Prokop
  252           milan	Milan Kupcevic
  253         mitya57	Dmitry Shachnev
  254        mjeanson	Michael Jeanson
  255           mjg59	Matthew Garrett
  256             mjr	Mark J Ray
  257           mones	Ricardo Mones Lastra
  258           moray	Moray Allan
  259           morph	Sandro Tosi
  260         mpalmer	Matthew Palmer
  261           mpitt	Martin Pitt
  262          mstone	Michael Stone
  263     mtecknology	Michael Lustfield
  264        mtmiller	Mike Miller
  265           murat	Murat Demirten
  266            mwei	Ming-ting Yao Wei
  267        mwhudson	Michael Hudson-Doyle
  268            myon	Christoph Berg
  269    natureshadow	Dominik George
  270           neilm	Neil McGovern
  271          nirgal	Jean-Michel Vourgère
  272           noahm	Noah Meyerhans
  273            noel	Noèl Köthe
  274         nomeata	Joachim Breitner
  275         noodles	Jonathan McDowell
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276        nthykier	Niels Thykier
  277           ntyni	Niko Tyni
  278            odyx	Didier Raboud
  279           ohura	Makoto OHURA
  280           olasd	Nicolas Dandrimont
  281         olebole	Ole Streicher
  282            olly	Olly Betts
  283          ondrej	Ondrej Sury
  284         onlyjob	Dmitry Smirnov
  285           onovy	Ondřej Nový
  286              p2	Peter De Schrijver
  287            pabs	Paul Wise
  288    paddatrapper	Kyle Robbertze
  289            pape	Gerrit Pape
  290        paravoid	Faidon Liambotis
  291            paul	Paul Slootman
  292         paulliu	Ying-Chun Liu
  293         paultag	Paul Richards Tagliamonte
  294             peb	Pierre-Elliott Bécue
  295           philh	Philip Hands
  296            piem	Paul Brossier
  297            pini	Gilles Filippini
  298            pino	Pino Toscano
  299           piotr	Piotr Ożarowski
  300             pjb	Phil Brooke
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301           pkern	Philipp Kern
  302          plessy	Charles Plessy
  303        plugwash	Peter Michael Green
  304       pmatthaei	Patrick Matthäi
  305          pmhahn	Philipp Matthias Hahn
  306           pollo	Louis-Philippe Véronneau
  307          pollux	Pierre Chifflier
  308        porridge	Marcin Owsiany
  309         praveen	Praveen Arimbrathodiyil
  310        preining	Norbert Preining
  311        pvaneynd	Peter Van Eynde
  312          rafael	Rafael Laboissiere
  313             ras	Russell Stuart
  314          rbasak	Robie Basak
  315             reg	Gregory Colpart
  316         reichel	Joachim Reichel
  317         rfehren	Roland Fehrenbacher
  318      rfrancoise	Romain Francoise
  319          richih	Richard Michael Hartmann
  320            riku	Riku Voipio
  321        rmayorga	Rene Mayorga
  322         roberto	Roberto C. Sanchez
  323          roland	Roland Rosenfeld
  324            rosh	Roger Shimizu
  325        rousseau	Ludovic Rousseau
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326             rra	Russ Allbery
  327             rrs	Ritesh Raj Sarraf
  328     rvandegrift	Ross Vandegrift
  329        santiago	Santiago Ruano Rincón
  330         sanvila	Santiago Vila
  331            sasa	Attila Szalay
  332         sathieu	Mathieu Parent
  333           satta	Sascha Steinbiss
  334        schultmc	Michael C. Schultheiss
  335         seamlik	Kai-Chung Yan
  336             seb	Sebastien Delafond
  337       sebastien	Sébastien Villemot
  338         serpent	Tomasz Rybak
  339           sesse	Steinar H. Gunderson
  340              sf	Stefan Fritsch
  341        sgolovan	Sergei Golovan
  342         sharkey	Eric Sharkey
  343             sjr	Simon Richter
  344           skitt	Stephen Kitt
  345           slomo	Sebastian Dröge
  346            smcv	Simon McVittie
  347             smr	Steven Michael Robbins
  348           smurf	Matthias Urlichs
  349       spwhitton	Sean Whitton
  350       sramacher	Sebastian Ramacher
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351            srud	Sruthi Chandran
  352          ssgelm	Stephen Gelman
  353      stapelberg	Michael Stapelberg
  354        stappers	Geert Stappers
  355          steele	David Steele
  356        stefanor	Stefano Rivera
  357           steve	Steve Kostecke
  358         stevenc	Steven Chamberlain
  359       sthibault	Samuel Thibault
  360             sto	Sergio Talens-Oliag
  361          stuart	Stuart Prescott
  362            sune	Sune Vuorela
  363       sunweaver	Mike Gabriel
  364           sur5r	Jakob Haufe
  365           szlin	SZ Lin
  366            tach	Taku Yasui
  367          taffit	David Prévot
  368          takaki	Takaki Taniguchi
  369           taowa	Taowa Munene-Tardif
  370             tbm	Martin Michlmayr
  371         tehnick	Boris Pek
  372        terceiro	Antonio Terceiro
  373              tg	Thorsten Glaser
  374         thansen	Tobias Hansen
  375         thibaut	Thibaut Jean-Claude Paumard
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  376           thijs	Thijs Kinkhorst
  377             thk	Thomas Koch
  378          tianon	Tianon Gravi
  379          tijuca	Carsten Schoenert
  380            timo	Timo Jyrinki
  381            tiwe	Timo Weingärtner
  382        tjaalton	Timo Aaltonen
  383        tjhukkan	Teemu Hukkanen
  384        tmancill	Tony Mancill
  385            tobi	Tobias Frost
  386           toddy	Tobias Quathamer
  387            toni	Toni Mueller
  388             tpo	Tomas Pospisek
  389         treinen	Ralf Treinen
  390           troyh	Troy Heber
  391        tvincent	Thomas Vincent
  392          tweber	Thomas Weber
  393           tytso	Theodore Y. Ts'o
  394            ucko	Aaron M. Ucko
  395        ukleinek	Uwe Kleine-König
  396          ulrike	Ulrike Uhlig
  397       ultrotter	Guido Trotter
  398        umlaeute	IOhannes m zmölnig
  399           urbec	Judit Foglszinger
  400         vagrant	Vagrant Cascadian
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  401        valhalla	Elena Grandi
  402         vasudev	Vasudev Sathish Kamath
  403          vcheng	Vincent Cheng
  404           viiru	Arto Jantunen
  405         vlegout	Vincent Legout
  406          vorlon	Steve Langasek
  407           vseva	Victor Seva
  408          vvidic	Valentin Vidic
  409          wagner	Hanno Wagner
  410           waldi	Bastian Blank
  411          weasel	Peter Palfrader
  412        weinholt	Göran Weinholt
  413           wferi	Ferenc Wágner
  414          wookey	Wookey
  415          wouter	Wouter Verhelst
  416            wrar	Andrey Rahmatullin
  417            xnox	Dimitri John Ledkov
  418            yadd	Xavier Guimard
  419         yyabuki	Yukiharu Yabuki
  420            zack	Stefano Zacchiroli
  421            zeha	Christian Hofstaedtler
  422            zhsj	Shengjing Zhu
  423            zigo	Thomas Goirand
  424           zobel	Martin Zobel-Helas
  425       zugschlus	Marc Haber

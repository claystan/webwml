<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the interpreter for the
Ruby language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10663">CVE-2020-10663</a>

    <p>Jeremy Evans reported an unsafe object creation vulnerability in the
    json gem bundled with Ruby. When parsing certain JSON documents, the
    json gem can be coerced into creating arbitrary objects in the
    target system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10933">CVE-2020-10933</a>

    <p>Samuel Williams reported a flaw in the socket library which may lead
    to exposure of possibly sensitive data from the interpreter.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.5.5-3+deb10u2.</p>

<p>We recommend that you upgrade your ruby2.5 packages.</p>

<p>For the detailed security status of ruby2.5 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby2.5">https://security-tracker.debian.org/tracker/ruby2.5</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4721.data"
# $Id: $

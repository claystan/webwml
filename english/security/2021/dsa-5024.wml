<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was found that Apache Log4j2, a Logging Framework for Java, did not protect
from uncontrolled recursion from self-referential lookups. When the logging
configuration uses a non-default Pattern Layout with a Context Lookup (for
example, $${ctx:loginId}), attackers with control over Thread Context Map (MDC)
input data can craft malicious input data that contains a recursive lookup,
resulting in a denial of service.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.17.0-1~deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.17.0-1~deb11u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5024.data"
# $Id: $

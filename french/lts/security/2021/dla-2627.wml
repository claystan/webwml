#use wml::debian::translation-check translation="115e4cc20409db2a26177e425d592f59a2dcbd6f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn a découvert qu’il existait une insuffisance de validation
d’entrée dans l’affichage du serveur X.Org.</p>

<p>Des vérifications insuffisantes dans les longueurs de requête
ChangeFeedbackControl de l’extension XInput pourraient avoir mené à un accès en
mémoire hors limites dans le serveur X. Ces problèmes peuvent conduire à une
élévation des privilèges pour les clients autorisés, en particulier dans les
systèmes où le serveur X est en fonctionnement comme utilisateur privilégié.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3472">CVE-2021-3472</a>

<p>Correction du dépassement de capacité inférieur de la requête
XChangeFeedbackControl().</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:1.19.2-1+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xorg-server.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2627.data"
# $Id: $

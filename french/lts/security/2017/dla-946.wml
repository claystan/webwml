#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque NSS est vulnérable à deux problèmes de sécurité :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5461">CVE-2017-5461</a>

<p>écriture hors limites dans l’encodage Base64. Cela peut déclencher un
plantage (déni de service) et peut être exploitable pour une exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5462">CVE-2017-5462</a>

<p>défaut dans la génération de nombre DRBG où l’état interne V ne transfère
pas correctement les bits.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2:3.26-1+debu7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-946.data"
# $Id: $

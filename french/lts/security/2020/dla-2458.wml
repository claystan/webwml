#use wml::debian::translation-check translation="78cf9e6fd6ae6b94be25548954275d9f9d3d904a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Drupal, un cadriciel complet de
gestion de contenu.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13666">CVE-2020-13666</a>

<p>L’interface de programmation applicative AJAX de Drupal ne désactivait pas
JSONP par défaut. Cela pourrait conduire à un script intersite.</p>

<p>Pour les configurations reposant sur l’API AJAX de Drupal pour les
requêtes JSONP, soit JSONP doit être réactivé, soit l’API AJAX jQuery doit être
utilisé.</p>

<p>Consultez l’annonce de l’amont pour plus de détails :
<a href="https://www.drupal.org/sa-core-2020-007">https://www.drupal.org/sa-core-2020-007</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13671">CVE-2020-13671</a>

<p>Drupal échouait à nettoyer les noms de fichier des fichiers téléversés. Cela
pourrait conduire à ce que des fichiers soient servis avec un mauvais type MIME
ou exécutés selon la configuration du serveur.</p>

<p>Il est aussi recommandé de vérifier les fichiers déjà téléversés pour des
extensions malveillantes. Pour plus de détails, consultez l’annonce de l’amont :
<a href="https://www.drupal.org/sa-core-2020-012">https://www.drupal.org/sa-core-2020-012</a>.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 7.52-2+deb9u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets drupal7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de drupal7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/drupal7">https://security-tracker.debian.org/tracker/drupal7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2458.data"
# $Id: $

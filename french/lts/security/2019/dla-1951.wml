#use wml::debian::translation-check translation="821f3b05bbba812ec42cb6a43be23b5e49fe428f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de déni de service dans la bibliothèque de
chiffrement libtomcrypt.</p>

<p>Une lecture hors limites et un plantage pourraient se produire à cause de
données encodées « DER » soigneusement contrefaites (par exemple, en important un
certificat X.509).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17362">CVE-2019-17362</a>

<p>Dans LibTomCrypt jusqu’à 1.18.2, la fonction der_decode_utf8_string (dans
der_decode_utf8_string.c) ne détectait pas correctement certaines séquences
UTF-8 non valables. Cela permettait selon le contexte à des attaquants de
provoquer un déni de service (lecture hors limites et plantage) ou de lire des
informations d’autres emplacements mémoire à l’aide de données encodées DER
soigneusement contrefaites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.17-6+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libtomcrypt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1951.data"
# $Id: $

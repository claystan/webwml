#use wml::debian::translation-check translation="72ca4a1def2e348d9a76c16854139b40c25c74d3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans fribidi, une implémentation
libre de l'algorithme Unicode BiDi. Ils sont liés à un dépassement de pile,
un dépassement de tas et un SEGV.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25308">CVE-2022-25308</a>

<p>Un problème de dépassement de pile dans main()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25309">CVE-2022-25309</a>

<p>Un problème de dépassement de tas dans fribidi_cap_rtl_to_unicode()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25310">CVE-2022-25310</a>

<p>Un problème de SEGV dans fribidi_remove_bidi_marks()</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.19.7-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets fribidi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de fribidi, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/fribidi">\
https://security-tracker.debian.org/tracker/fribidi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2974.data"
# $Id: $

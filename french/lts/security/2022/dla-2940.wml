#use wml::debian::translation-check translation="ebffc86e4b020fb91261eeb10e946c7d8669736e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
peuvent conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3640">CVE-2021-3640</a>

<p>Lin Ma a découvert une situation de compétition dans l'implémentation du
protocole Bluetooth SCO qui peut conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3752">CVE-2021-3752</a>

<p>Likang Luo de NSFOCUS Security Team a découvert un défaut dans
l'implémentation de Bluetooth L2CAP qui peut conduire à une utilisation de
mémoire après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4002">CVE-2021-4002</a>

<p>hugetlbfs, le système de fichiers virtuel utilisé par les applications
pour allouer de très grandes pages dans la RAM, ne vidait pas le TLB du
processus dans un cas où cela était nécessaire. Dans certaines
circonstances, un utilisateur local pouvait être capable de lire et
d'écrire ces très grandes pages après qu'elles ont été libérées et
réallouées à un processus différent. Cela pouvait conduire à une élévation
de privilèges, un déni de service ou des fuites d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4083">CVE-2021-4083</a>

<p>Jann Horn a signalé une situation de compétition dans le ramasse-miettes
des sockets locaux (Unix), qui peut conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

<p>Kirill Tkhai a découvert une fuite de données dans la manière dont
l'IOCTL XFS_IOC_ALLOCSP dans le système de fichiers XFS permettait une
augmentation de taille de fichiers ayant une taille non alignée. Un
attaquant pouvait tirer avantage de ce défaut pour une fuite de données sur
le système de fichiers XFS</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4202">CVE-2021-4202</a>

<p>Lin Ma a découvert des situations de compétition dans le pilote NCI
(NFC Controller Interface), qui pouvaient conduire à une utilisation de
mémoire après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement une élévation de privilèges.</p>

<p>Ce protocole n'est pas activé dans les configurations du noyau officiel
de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)</p>

<p>Juergen Gross a signalé que des dorsaux de PV malveillants peuvent
provoquer un déni de service dans les clients servis par ces dorsaux au
moyen d'événements à haute fréquence, même si ces dorsaux sont exécutés
dans un environnement moins privilégié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)</p>

<p>Juergen Gross a découvert que les clients Xen peuvent contraindre le
pilote netback de Linux à accaparer une grande quantité de mémoire du
noyau, avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29264">CVE-2021-29264</a>

<p>Le pilote "gianfar" Ethernet, utilisé avec quelques SoC Freescale, ne
gérait pas correctement un dépassement de file d’attente Rx quand les
paquets "jumbo" étaient activés. Sur les systèmes utilisant ce pilote et
des paquets jumbo, un attaquant sur le réseau pourrait exploiter cela pour
provoquer un déni de service (plantage).</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33033">CVE-2021-33033</a>

<p>L'outil syzbot a découvert un bogue de compte de références dans
l'implémentation de CIPSO qui peut conduire à une utilisation de mémoire
après libération.</p>

<p>Ce protocole n'est pas activé dans les configurations du noyau officiel
de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

<p>Szymon Heidrich a découvert une vulnérabilité de dépassement de tampon
dans le sous-système gadget USB, avec pour conséquences la divulgation
d'informations, un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39686">CVE-2021-39686</a>

<p>Une situation de compétition a été découverte dans le pilote de création
de lien d'Android, qui pouvait conduire à des vérifications de sécurité
incorrectes. Sur les systèmes où le pilote de création de lien est chargé,
un utilisateur local pouvait exploiter cela pour une élévation de
privilèges.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau  officiel de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39698">CVE-2021-39698</a>

<p>Linus Torvalds a signalé un défaut dans l'implémentation de la
scrutation (« polling ») de fichiers, qui pouvait conduire à une
utilisation de mémoire après libération. Un utilisateur local pouvait
exploiter cela pour un déni de service (corruption de mémoire ou plantage)
ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39714">CVE-2021-39714</a>

<p>Un possible dépassement de compte de références a été découverte dans le
pilote Ion d'Android. Sur les systèmes où le pilote Ion est chargé, un
utilisateur local pouvait exploiter cela pour un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation de
privilèges.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

<p>Zekun Shen et Brendan Dolan-Gavitt ont découvert un défaut dans la
fonction mwifiex_usb_recv() du pilote WiFi-Ex USB de Marvell. Un
attaquant capable de se connecter à un périphérique USB contrefait peut
tirer avantage de ce défaut pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

<p>Le pilote du protocole Phone Network (PhoNet) présente une fuite de
nombre de références dans la fonction pep_sock_accept().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>

<p>(INTEL-SA-00598)</p>

<p>Des chercheurs de VUSec ont découvert que le tampon « Branch History »
des processeurs Intel peut être exploité pour créer des attaques par canal
auxiliaire d'information avec une exécution spéculative. Ce problème est
semblable à Spectre variante 2, mais demande des palliatifs supplémentaires
sur certains processeurs.</p>

<p>Cela peut être exploité pour obtenir des informations sensibles à
partir d'un contexte de sécurité différent, comme à partir de l'espace
utilisateur vers le noyau ou à partir d'un client KVM vers le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a>

<p>(INTEL-SA-00598)</p>

<p>Il s'agit d'un problème similaire à <a
href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>,
mais recouvre une exploitation à l'intérieur d'un contexte de sécurité,
comme à partir de code compilé avec JIT dans un bac à sable vers du code de
l'hôte dans le même processus.</p>

<p>Ce problème est partiellement pallié en désactivant eBPF pour les
utilisateurs non privilégiés avec l'option sysctl :
kernel.unprivileged_bpf_disabled=2. Cette mise à jour fait cela par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

<p>Sushma Venkatesh Reddy a découvert une absence de vidage de TLB du GPU
dans le pilote i915, ayant pour conséquences un déni de service ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

<p>Samuel Page et Eric Dumazet ont signalé un dépassement de pile dans le
module réseau pour le protocole TIPC (Transparent Inter-Process
Communication), ayant pour conséquences un déni de service ou
éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

<p>Une utilisation de mémoire après libération a été découverte dans le
pilote de prise en charge du contrôleur hôte MOXART SD/MMC. Ce défaut
n'impacte pas les paquets binaires de Debian dans la mesure où
CONFIG_MMC_MOXART n'est pas configuré</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

<p>Yiqi Sun et Kevin Wang ont signalé que le sous-système cgroup-v1 ne
restreint pas correctement l'accès à la fonction « release-agent ». Un
utilisateur local peut tirer avantage de ce défaut pour une élévation de
privilèges et le contournement de l'isolation d'espace de noms.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

<p>butt3rflyh4ck a découvert un déréférencement de pointeur NULL dans le
système de fichiers UDF. Un utilisateur local qui peut monter une image UDF
contrefaite pour l'occasion peut utiliser ce défaut pour planter le
système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

<p>Lyu Tao a signalé un défaut dans l'implémentation de NFS dans le noyau
Linux lors du traitement de requêtes pour ouvrir un répertoire sur un
fichier ordinaire, qui pouvait avoir pour conséquence une fuite
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

<p>Szymon Heidrich a signalé que le sous-système USB Gadget manque de
certaines validations des requêtes du descripteur d'OS de l'interface, avec
pour conséquence une corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

<p>Szymon Heidrich a signalé que le gadget USB RNDIS manque de validation
de la taille de la commande RNDIS_MSG_SET, avec pour conséquence une fuite
d'informations à partir de la mémoire du noyau.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.9.303-1. Cette mise à jour inclut en complément beaucoup d'autres
corrections de bogues issues des mises à jour des versions de stable de
4.9.291 à 4.9.303 comprises.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2940.data"
# $Id: $

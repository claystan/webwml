#use wml::debian::translation-check translation="801a78a1d5593a426b9dabc3412803e9ded02cab" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une situation de compétition potentielle dans Paramiko, une
implémentation de l'algorithme de SSH en Python pur. En particulier, une
divulgation non autorisée d'informations aurait pu se produire lors de la
création des clés privés SSH.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24302">CVE-2022-24302:
Dans les versions de Paramiko antérieures à 2.10.1, une situation de
compétition (entre la création et la commande chmod) dans la fonction
write_private_key_file pouvait permettre la divulgation non autorisée
d'informations.</a>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.0.0-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets paramiko.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2959.data"
# $Id: $

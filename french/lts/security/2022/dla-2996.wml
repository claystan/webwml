#use wml::debian::translation-check translation="0eb462e2387c29071f7d2f40467ed5e1088b2a4c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans mruby, une
implémentation légère du langage Ruby ayant pour conséquence des dénis de
service, des dépassements de tampon, l'exécution de code arbitraire ou
éventuellement avoir d’autres conséquences indéterminées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9527">CVE-2017-9527</a>

<p>La fonction mark_context_stack dans gc.c de mruby jusqu'à la
version  1.2.0 permet à des attaquants de provoquer un déni de service
(utilisation de mémoire après libération basée sur le tas et plantage de
l'application) ou éventuellement avoir d’autres conséquences indéterminées
au moyen d'un fichier .rb contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10191">CVE-2018-10191</a>

<p>Dans les versions de mruby jusqu'à y compris 1.4.0, il y a un
dépassement d'entier dans src/vm.c::mrb_vm_exec() lors de la gestion
d'OP_GETUPVAR en présence de « scopes » profondément imbriqués, ayant pour
conséquence une utilisation de mémoire après libération. Un attaquant qui
peut exécuter du code Ruby peut utiliser cela pour éventuellement exécuter
du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11743">CVE-2018-11743</a>

<p>La fonction init_copy dans kernel.c de mruby 1.4.1 réalise des appels
initialize_copy pour les objets TT_ICLASS, ce qui permet à des attaquants
de provoquer un déni de service (pointeur mrb_hash_keys non initialisé et
plantage de l'application) éventuellement avoir d’autres conséquences
indéterminées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12249">CVE-2018-12249</a>

<p>Un problème a été découvert dans mruby 1.4.1. Il y a un déréférencement
de pointeur NULL dans mrb_class_real parce que « class BasicObject » n'est
pas correctement pris en charge dans class.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14337">CVE-2018-14337</a>

<p>La macro CHECK dans mrbgems/mruby-sprintf/src/sprintf.c de mruby 1.4.1
contient un dépassement d'entier signé, menant potentiellement à un accès
mémoire hors limites parce que la fonction mrb_str_resize dans string.c ne
vérifie pas les longueurs négatives.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15866">CVE-2020-15866</a>

<p>mruby jusqu'à la version 2.1.2-rc a un dépassement de tampon basé sur le
tas dans la fonction mrb_yield_with_class de vm.c à cause d'une gestion
incorrecte de la pile VM. Il peut être déclenché au moyen de la fonction
stack_copy.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.2.0+20161228+git30d5424a-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mruby.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mruby, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mruby">\
https://security-tracker.debian.org/tracker/mruby</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2996.data"
# $Id: $

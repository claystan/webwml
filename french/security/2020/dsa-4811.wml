#use wml::debian::translation-check translation="1e5f62f34bcc8da30f41325ca8cb664f8713412c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>La liste noire par défaut de XStream, une bibliothèque Java pour
sérialiser des objets vers et depuis XML, était vulnérable à l'exécution de
commandes d'interpréteur arbitraires en manipulant le flux d'entrée traité.</p>

<p>Pour une défense en profondeur supplémentaire, il est recommandé de
passer à l'approche par liste blanche du cadriciel de sécurité d’XStream.
Pour davantage d'informations, veuillez consulter
<a href="https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2">\
https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2</a></p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.4.11.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4811.data"
# $Id: $

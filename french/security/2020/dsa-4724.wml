#use wml::debian::translation-check translation="47c9da48bb2e6ee7f5d51bb1bfb0e169808fcbf0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9802">CVE-2020-9802</a>

<p>Samuel Gross a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9803">CVE-2020-9803</a>

<p>Wen Xu a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9805">CVE-2020-9805</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pourrait conduire à une attaque de script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9806">CVE-2020-9806</a>

<p>Wen Xu a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9807">CVE-2020-9807</a>

<p>Wen Xu a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9843">CVE-2020-9843</a>

<p>Ryan Pickren a découvert que le traitement d'un contenu web contrefait
pourrait conduire à une attaque de script intersite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9850">CVE-2020-9850</a>

<p>@jinmo123, @setuid0x0_ et @insu_yun_en ont découvert qu'un attaquant
distant peut être capable de provoquer l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13753">CVE-2020-13753</a>

<p>Milan Crha a découvert qu'un attaquant peut être capable d'exécuter des
commandes en dehors du bac à sable de bubblewrap.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.28.3-2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4724.data"
# $Id: $

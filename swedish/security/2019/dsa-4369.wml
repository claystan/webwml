#use wml::debian::translation-check translation="8a1c0e346cc4b60809eb2067ebcb114fe8cc027d" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Xen hypervisor:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19961">CVE-2018-19961</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19962">CVE-2018-19962</a>

	<p>Paul Durrant upptäckte att felaktig TLB-hantering kunde resultera i
	överbelastning, utökning av privilegier eller informationsläckage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19965">CVE-2018-19965</a>

	<p>Matthew Daley upptäckte att felaktig hantering av instruktionen
	INVPCID kunde resultera i överbelastning av PV-gäster.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19966">CVE-2018-19966</a>

	<p>Man har upptäckt att en regression i rättningen för att adressera
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>
	kunde resultera i överbelastning, rättighetseskalering eller
	informationsläckage av en PV-gäst.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19967">CVE-2018-19967</a>

	<p>Man har upptäckt att ett fel i vissa Intel-CPUer kunde resultera
	i överbelastning genom en gästinstans.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 4.8.5+shim4.10.2+xsa282-1+deb9u11.</p>

<p>Vi rekommenderar att ni uppgraderar era xen-paket.</p>

<p>För detaljerad säkerhetsstatus om xen vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4369.data"

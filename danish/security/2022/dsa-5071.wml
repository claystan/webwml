#use wml::debian::translation-check translation="7000f208a4e5ab2d7a872d4439db6ec5dcba9270" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i Samba, en SMB/CIFS-fil-, print- og 
loginserver til Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44142">CVE-2021-44142</a>

    <p>Orange Tsai rapporterede om en heapsårbarhed i forbindelse skrivning
    udenfor grænserne i VFS-modulet vfs_fruit, hvilken kunne medføre 
    fjernudførelse af vilkårlig kode som root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">CVE-2022-0336</a>

    <p>Kees van Vloten rapporterede at Sambas AD-bruger med rettighed til at 
    skrive til en konto, kunne udgive sig for at være vilkårlige 
    tjenester.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet i 
version 2:4.9.5+dfsg-5+deb10u3.  Jævnfør 5015-1, er 
<a href="https://security-tracker.debian.org/tracker/CVE-2022-0336">\
CVE-2022-0336</a> ikke løst i den gamle stabile distribution (buster).</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i version 
2:4.13.13+dfsg-1~deb11u3.  Yderligere er der nogle opfølgende rettelser til 
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25717">\
CVE-2020-25717</a> med i denne opdatering (jævnfør #1001068).</p>

<p>Vi anbefaler at du opgraderer dine samba-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende samba, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5071.data"
